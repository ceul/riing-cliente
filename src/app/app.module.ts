import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsModule } from '../app/components/components.module';
import { ToastMessagesComponent } from '../app/components/toast-messages/toast-messages.component';
import { LoadingSpinnerService } from './services/loading-spinner/loading-spinner.service';
import { LayoutModule } from './layout/layout.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PipesModule } from './pipes/pipes.module';
import { AlertModule } from 'ngx-bootstrap/alert';
import { HttpClientModule } from '@angular/common/http';
import { ErrorsModule } from './modules/error-handler/errors.module';
import { SocketService } from './services/socket/socket.service';
import { JwtHelperService, JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    ToastMessagesComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    ComponentsModule,
    LayoutModule,
    BsDropdownModule.forRoot(),
    BrowserAnimationsModule,
    PipesModule.forRoot(),
    AlertModule.forRoot(),
    HttpClientModule,
    ErrorsModule,
    ServiceWorkerModule.register('custom-service-worker.js', { enabled: environment.production }),
    RouterModule,
  ],
  providers: [
    LoadingSpinnerService,
    SocketService,
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService,
    Title
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
