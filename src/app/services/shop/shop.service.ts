import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShopService extends BaseServiceService {

	className = 'ShopService'

	constructor(public http: HttpClient) {
		super(http)
	}

	get() {
		try {
			return this.consumeAPI('/shop/get');
		} catch (e) {
			console.log(`An error occurred getting shop ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/shop/getById',{ id });
		} catch (e) {
			console.log(`An error occurred getting shop by id ${this.getById.name} --> ${this.className}`)
		}
	}

	getByType(type: string) {
		try {
			return this.consumeAPI('/shop/getByType',{ type });
		} catch (e) {
			console.log(`An error occurred getting shop by type ${this.getByType.name} --> ${this.className}`)
		}
	}
}
