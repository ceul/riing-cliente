import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import OrdenModel from '../../models/orden.model';

@Injectable({
  providedIn: 'root'
})
export class OrdenesService extends BaseServiceService {

	className = 'OrderService'

	constructor(public http: HttpClient) {
		super(http)
	}

	save(data: OrdenModel) {
		try {
			return this.consumeAPI('/order', data);
		} catch (e) {
			console.log(`An error occurred saving order ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/order/get');
		} catch (e) {
			console.log(`An error occurred getting orders: ${this.get.name} --> ${this.className}`)
		}
	}

	getProcess() {
		try {
			return this.consumeAPI('/order/getProcess');
		} catch (e) {
			console.log(`An error occurred getting on process orders: ${this.getProcess.name} --> ${this.className}`)
		}
	}

	getReadyToDeliver() {
		try {
			return this.consumeAPI('/order/getReadyToDeliver');
		} catch (e) {
			console.log(`An error occurred getting on process orders: ${this.getReadyToDeliver.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/order/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting order by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	getByClient(client: string) {
		try {
			return this.consumeAPI('/order/getByClient', { client });
		} catch (e) {
			console.log(`An error occurred getting order by client: ${this.getByClient.name} --> ${this.className}`)
		}
	}

	update(data: OrdenModel) {
		try {
			return this.consumeAPI('/order/update', data);
		} catch (e) {
			console.log(`An error occurred updatting order: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/order/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting order: ${this.delete.name} --> ${this.className}`)
		}
	}
}
