import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import LogModel from '../../models/log.model';

@Injectable({
  providedIn: 'root'
})
export class LogService extends BaseServiceService {

	className = 'LogService'
	
	constructor(public http: HttpClient) {
		super(http)
	}


	save(log: LogModel) {
		try {
			return this.consumeAPI('/log', log);
		} catch (e) {
			console.log(`An error occurred saving the log ${this.save.name} --> ${this.className}`)
		}
	}
}