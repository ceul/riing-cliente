import { Injectable, Output, EventEmitter } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import UsuarioModel from '../../models/usuario.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseServiceService {

  className = 'AuthService';

  @Output() isLogged:  EventEmitter<any> = new EventEmitter(); 

	constructor(public http: HttpClient) {
		super(http)
	}

  val(data: UsuarioModel) {
    try {
      return this.consumeAPI('/user/val', { email: data.email, contrasena: data.contrasena });
    } catch (e) {
      console.log(`An error has occurred validating the user: ${this.val.name} --> ${this.className}`)
    }
  }

  isLoggedFunc(isLogged: boolean){
    try {
      this.isLogged.emit(isLogged)
    } catch (e) {
      console.log(`An error has occurred validating the user: ${this.isLoggedFunc.name} --> ${this.className}`)
    }
  }


}
