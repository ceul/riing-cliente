import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import UsuarioModel from '../../models/usuario.model';

@Injectable({
	providedIn: 'root'
})
export class UserService extends BaseServiceService {

	className = 'UserService'

	constructor(public http: HttpClient) {
		super(http)
	}


	save(data: UsuarioModel) {
		try {
			return this.consumeAPI('/user', data);
		} catch (e) {
			console.log(`An error occurred saving user ${this.save.name} --> ${this.className}`)
		}

	}

	update(data: UsuarioModel) {
		try {
			return this.consumeAPI('/user/update', data);
		} catch (e) {
			console.log(`An error occurred updatting user ${this.update.name} --> ${this.className}`)
		}
	}

	resetPassword(email: string) {
		try {
			return this.consumeAPI('/user/resetPassword', { email });
		} catch (e) {
			console.log(`An error occurred resetting Password ${this.resetPassword.name} --> ${this.className}`)
		}
	}

	changePassword(id: string, newPassword: string) {
		try {
			return this.consumeAPI('/user/changePassword', { id, newPassword });
		} catch (e) {
			console.log(`An error occurred changing Password ${this.resetPassword.name} --> ${this.className}`)
		}
	}

	isChangePasswordId(id: string) {
		try {
			return this.consumeAPI('/user/isChangePasswordId', { id });
		} catch (e) {
			console.log(`An error occurred isChangePasswordId ${this.isChangePasswordId.name} --> ${this.className}`)
		}
	}

}