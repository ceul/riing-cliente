import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import UbicacionModel from '../../models/ubicacion.model';

@Injectable({
	providedIn: 'root'
})
export class UbicacionesService extends BaseServiceService {

	className = 'UbicacionesService'

	constructor(public http: HttpClient) {
		super(http)
	}

	save(ubicacion: UbicacionModel) {
		try {
			return this.consumeAPI('/ubicacion', ubicacion);
		} catch (e) {
			console.log(`An error occurred getting ubicacion ${this.save.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/ubicacion/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting ubicacion by id ${this.getById.name} --> ${this.className}`)
		}
	}

	getByUser(userId: string) {
		try {
			return this.consumeAPI('/ubicacion/getByUser', { userId });
		} catch (e) {
			console.log(`An error occurred getting ubicacion by userId ${this.getByUser.name} --> ${this.className}`)
		}
	}

	update(ubicacion: UbicacionModel) {
		try {
			return this.consumeAPI('/ubicacion/update', ubicacion);
		} catch (e) {
			console.log(`An error occurred updatting ubicacion ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/ubicacion/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting ubicacion ${this.delete.name} --> ${this.className}`)
		}
	}

}