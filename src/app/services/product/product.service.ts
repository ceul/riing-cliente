import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import ProductModel from '../../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends BaseServiceService {

	className = 'ProductService'

	constructor(public http: HttpClient) {
		super(http)
	}

	get() {
		try {
			return this.consumeAPI('/product/get');
		} catch (e) {
			console.log(`An error occurred saving product ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/product/getById', { id });
		} catch (e) {
			console.log(`An error occurred saving product ${this.getById.name} --> ${this.className}`)
		}
	}

	getByCategory(categoria: string, almacen: string) {
		try {
			return this.consumeAPI('/product/getByCategory', { categoria, almacen });
		} catch (e) {
			console.log(`An error occurred saving product ${this.getByCategory.name} --> ${this.className}`)
		}
	}

	getProductByShop(almacen: string) {
		try {
			return this.consumeAPI('/product/getByShop', {almacen});
		} catch (e) {
			console.log(`An error occurred saving product ${this.getProductByShop.name} --> ${this.className}`)
		}
	}

}
