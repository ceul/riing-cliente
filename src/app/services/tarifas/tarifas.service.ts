import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseServiceService } from '../base-services/base-service.service';

@Injectable({
  providedIn: 'root'
})
export class TarifasService extends BaseServiceService {

	className = 'TarifasService'

	constructor(public http: HttpClient) {
		super(http)
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/tarifa/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting tarifa by id ${this.getById.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/tarifa/get');
		} catch (e) {
			console.log(`An error occurred getting tarifas ${this.get.name} --> ${this.className}`)
		}
	}
}
