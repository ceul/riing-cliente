import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import * as io from 'socket.io-client';
import { AuthBusiness } from '../../business/user/auth.business';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private socket

  constructor(private authBusiness: AuthBusiness) {
    
  }

  listen(eventName: string) {
    return new Observable((subscriber) => {
      this.socket.on(eventName, (data) => {
        subscriber.next(data)
      })
    })
  }

  emit(eventName: string, data: any) {
    this.socket.emit(eventName, data)
  }

  async setupSocketConnection() {
    this.socket = io(`${environment.url}:${environment.port}`);
    await this.autenticateUser()
  }

  async autenticateUser(){
    let token = await this.authBusiness.readToken()
    if (token !== '') {
      this.emit('authUser', token)
    }
  }

}