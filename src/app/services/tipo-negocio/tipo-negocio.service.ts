import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import TipoNegocioModel from '../../models/tipo-negocio.model';

@Injectable({
  providedIn: 'root'
})
export class TipoNegocioService extends BaseServiceService {

	className = 'TipoNegocioService'

	constructor(public http: HttpClient) {
		super(http)
	}

	get() {
		try {
			return this.consumeAPI('/shopType/get');
		} catch (e) {
			console.log(`An error occurred getting types ${this.get.name} --> ${this.className}`)
		}
	}

	getVisible() {
		try {
			return this.consumeAPI('/shopType/getVisible');
		} catch (e) {
			console.log(`An error occurred getting types ${this.getVisible.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/shopType/getById',{ id });
		} catch (e) {
			console.log(`An error occurred getting shopType by id ${this.getById.name} --> ${this.className}`)
		}
	}
}
