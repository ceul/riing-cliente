import { TestBed } from '@angular/core/testing';

import { TipoNegocioService } from './tipo-negocio.service';

describe('TipoNegocioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoNegocioService = TestBed.get(TipoNegocioService);
    expect(service).toBeTruthy();
  });
});
