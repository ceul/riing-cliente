import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import AlmacenModel from '../../models/almacen.model';

@Injectable({
  providedIn: 'root'
})
export class AlmacenService extends BaseServiceService{

  className = 'AlmacenService'

	constructor(public http: HttpClient) {
		super(http)
  }
  
	getById(id: string) {
		try {
			return this.consumeAPI('/shop/getById', {id});
		} catch (e) {
			console.log(`An error occurred getting shop by id: ${this.getById.name} --> ${this.className}`)
		}
	}

}
