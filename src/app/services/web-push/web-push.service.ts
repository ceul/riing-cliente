import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import WebPushModel from '../../models/web-push.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WebPushService extends BaseServiceService {

	className = 'WebPushService'

	constructor(public http: HttpClient) {
		super(http)
  }
  
  get() {
		try {
			return this.consumeAPI('/webPush/get');
		} catch (e) {
			console.log(`An error occurred getting notifications ${this.get.name} --> ${this.className}`)
		}
	}

	sendNotification(notification: WebPushModel) {
		try {
			return this.consumeAPI('/webPush/sendNotification', notification);
		} catch (e) {
			console.log(`An error occurred sending Notification ${this.sendNotification.name} --> ${this.className}`)
		}
  }
  
  insertSubscription(subscription: any) {
		try {
			return this.consumeAPI('/webPush/insertSubscription', {subscription});
		} catch (e) {
			console.log(`An error occurred inserting subscription ${this.insertSubscription.name} --> ${this.className}`)
		}
	}
}
