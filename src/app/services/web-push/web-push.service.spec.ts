import { TestBed } from '@angular/core/testing';

import { WebPushService } from './web-push.service';

describe('WebPushService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebPushService = TestBed.get(WebPushService);
    expect(service).toBeTruthy();
  });
});
