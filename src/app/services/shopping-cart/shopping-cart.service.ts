import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import ShoppingCartModel from '../../models/shopping-cart.model';
import RelProductoOrdenModel from '../../models/rel_producto_orden.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  @Output() emitProduct: EventEmitter<any> = new EventEmitter()

  constructor() { }

  addProduct(almacenId: string, producto: RelProductoOrdenModel) {
    try {
      let shoppingCart = <ShoppingCartModel[]>JSON.parse(localStorage.getItem('shoppingCart'))
      if (shoppingCart !== null && shoppingCart !== undefined) {
        let index = shoppingCart.findIndex(almacen => almacen.almacen == almacenId)
        if (index !== -1) {
          shoppingCart[index].orden.push(producto)
        } else {
          let newAlmacen = new ShoppingCartModel()
          newAlmacen.almacen = almacenId
          newAlmacen.orden = [producto]
          shoppingCart.push(newAlmacen)
        }
        localStorage.setItem('shoppingCart', JSON.stringify(shoppingCart))
        this.emitProduct.emit()
      } else {
        shoppingCart = []
        let newAlmacen = new ShoppingCartModel()
        newAlmacen.almacen = almacenId
        newAlmacen.orden = [producto]
        shoppingCart.push(newAlmacen)
        localStorage.setItem('shoppingCart', JSON.stringify(shoppingCart))
        this.emitProduct.emit()
      }
    } catch (error) {
      console.log(`An error ocurred on addProduct => ShoppingCartService`)
    }
  }

  deleteProduct(almacenId: string, productPosition: number) {
    try {
      let shoppingCart = <ShoppingCartModel[]>JSON.parse(localStorage.getItem('shoppingCart'))
      if (shoppingCart !== null && shoppingCart !== undefined) {
        let index = shoppingCart.findIndex(almacen => almacen.almacen == almacenId)
        if (index !== -1) {
          shoppingCart[index].orden.splice(productPosition, 1)
          localStorage.setItem('shoppingCart', JSON.stringify(shoppingCart))
          this.emitProduct.emit()
        }

      }
    } catch (error) {
      console.log(`An error ocurred on deleteProduct => ShoppingCartService`)
    }
  }
}
