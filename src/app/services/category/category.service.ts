import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseServiceService } from '../base-services/base-service.service';
import CategoryModel from '../../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService extends BaseServiceService {

	className = 'CategoryService'

	constructor(public http: HttpClient) {
		super(http)
	}

	get() {
		try {
			return this.consumeAPI('/category/get');
		} catch (e) {
			console.log(`An error occurred getting categories ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/category/getById',{ id });
		} catch (e) {
			console.log(`An error occurred getting category by id ${this.getById.name} --> ${this.className}`)
		}
  }
  
  getWithProductsByShop(almacen: string) {
		try {
			return this.consumeAPI('/category/getWithProductsByShop',{ almacen });
		} catch (e) {
			console.log(`An error occurred getting category with products by shop ${this.getWithProductsByShop.name} --> ${this.className}`)
		}
	}
}
