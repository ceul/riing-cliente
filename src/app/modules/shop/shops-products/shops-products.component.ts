import { Component, OnInit, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { LoadingSpinnerService } from '../../../services/loading-spinner/loading-spinner.service';
import { CategoryBusiness } from '../../../business/category/category.business';
import CategoryModel from '../../../models/category.model';
import { environment } from '../../../../environments/environment';
import AlmacenModel from '../../../models/almacen.model';
import { AlmacenBusiness } from '../../../business/almacen/almacen.business';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ProductDetailComponent } from '../../../components/product-detail/product-detail.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'riing-shops-products',
  templateUrl: './shops-products.component.html',
  styleUrls: ['./shops-products.component.scss']
})
export class ShopsProductsComponent implements OnInit {

  public shopId: string
  public shopType: string
  public categories: CategoryModel[]
  public shop: AlmacenModel
  private productDetailModal: BsModalRef
  public onTable: boolean

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {

    if (window.pageYOffset > 300) {
      let element = document.getElementById('categorybar');
      element.classList.add('sticky');
    } else {
      let element = document.getElementById('categorybar');
      element.classList.remove('sticky');
    }
  }

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private loadingService: LoadingSpinnerService,
    private categoryBusiness: CategoryBusiness,
    private modalService: BsModalService,
    private titleService: Title,
    private almacenBusiness: AlmacenBusiness
  ) { }

  async ngOnInit() {
    try {
      this.loadingService.show()
      let promises = []
      localStorage.setItem('deliveryWay', '1')
      await new Promise((resolve, reject) => {
        this.activatedRoute.params.pipe(take(1)).subscribe(async params => {
          this.shopId = params['shop'];
          this.shopType = params['shopType']
          resolve()
        })
      })

      await new Promise((resolve, reject) => {
        this.activatedRoute.url.pipe(take(1)).subscribe(async urlSegments => {
          if (urlSegments.length === 4) {
            this.onTable = urlSegments[3].path == 't' ? true : false
          } else {
            this.onTable = false
          }
          resolve()
        })
      })
      promises.push(this.categoryBusiness.getWithProductsByShop(this.shopId).then(categories => {
        this.categories = Object.values(categories)
      }))
      promises.push(this.almacenBusiness.getById(this.shopId).then(shop => {
        this.shop = Object.values(shop)[0]
        this.titleService.setTitle(`Mira lo que ${this.shop.usuario.nombre} en Santa Rosa De Cabal tiene para ti | Riing.com.co`)

      }))
      await Promise.all(promises)
      this.loadingService.hide()
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => ShopsProductsComponent`)
    }
  }

  goToCart() {
    try {
      this.router.navigateByUrl('/order')
    } catch (error) {
      console.log(`An error ocurred on goToCart => ShopsProductsComponent`)
    }
  }

  public navigateToTypes() {
    try {
      this.router.navigateByUrl(`/shop/types/${this.shopType}`)
    } catch (error) {
      console.log(`An error ocurred on navigateToShop => ShopsProductsComponent`)
    }
  }

  public getUrl(id) {
    try {
      return `${environment.url}:${environment.port}/product/getProductImage/${id}`
    } catch (error) {
      console.log(`An error ocurred on getUrl => ShopsProductsComponent`)
    }
  }

  getShopUrl(id) {
    try {
      return `${environment.url}:${environment.port}/shop/getLogo/${id}`
    } catch (error) {
      console.log(`An error ocurred on getUrl => ShopsProductsComponent`)
    }
  }

  openProductDetailModal(id) {
    try {
      this.productDetailModal = this.modalService.show(ProductDetailComponent, {
        class: 'modal-lg modal-dialog-centered',
        animated: true,
        initialState: {
          productId: id,
          isVisible: this.shop.visible
        }
      })
    } catch (error) {
      console.log(`An error ocurred on openproductDetailModal => HeaderComponent`)
    }
  }

}
