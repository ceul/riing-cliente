import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopsTypesComponent } from './shops-types.component';

describe('ShopsTypesComponent', () => {
  let component: ShopsTypesComponent;
  let fixture: ComponentFixture<ShopsTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopsTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopsTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
