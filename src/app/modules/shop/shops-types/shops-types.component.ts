import { Component, OnInit } from '@angular/core';
import { LoadingSpinnerService } from '../../../services/loading-spinner/loading-spinner.service';
import { Router } from '@angular/router';
import { TipoNegocioBusiness } from '../../../business/tipo-negocio/tipo-negocio.business';
import TipoNegocioModel from '../../../models/tipo-negocio.model';
import { environment } from '../../../../environments/environment';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'riing-shops-types',
  templateUrl: './shops-types.component.html',
  styleUrls: ['./shops-types.component.scss']
})
export class ShopsTypesComponent implements OnInit {

  public tiposNegocio: TipoNegocioModel[]
  constructor(
    private loadingService: LoadingSpinnerService,
    private router: Router,
    private titleService: Title,
    private tipoNegocioBusiness: TipoNegocioBusiness
  ) {

  }

  async ngOnInit() {
    try {
      this.titleService.setTitle(`Tipos de negocio | Riing.com.co`)
      this.loadingService.show()
      this.tiposNegocio = Object.values(await this.tipoNegocioBusiness.getVisible())
      this.loadingService.hide()
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => ShopsTypesComponent`)
    }
  }

  navigateToProducts(id: string) {
    try {
      this.router.navigateByUrl(`/shop/types/${id}`)
    } catch (error) {
      console.log(`An error ocurred on navigateToProducts => ShopsTypesComponent`)
    }
  }

  getUrl(id){
    try{
      return `${environment.url}:${environment.port}/shopType/getTypeImage/${id}`
    } catch(error){
      console.log(`An error ocurred on getUrl => ShopsTypesComponent`)
    }
  }

}
