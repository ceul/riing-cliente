import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingSpinnerService } from '../../../services/loading-spinner/loading-spinner.service';
import { ShopBusiness } from '../../../business/shop/shop.business';
import AlmacenModel from '../../../models/almacen.model';
import { environment } from '../../../../environments/environment';
import { take } from 'rxjs/operators';
import { TipoNegocioBusiness } from '../../../business/tipo-negocio/tipo-negocio.business';
import TipoNegocioModel from '../../../models/tipo-negocio.model';
import { Title } from '@angular/platform-browser';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
@Component({
  selector: 'riing-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.scss']
})
export class ShopsComponent implements OnInit {

  @ViewChild('shopClosed', { static: true }) public shopClosedModal: ModalDirective;

  public shops: AlmacenModel[]
  private typeId: string
  public type: TipoNegocioModel
  public collapseFilter: boolean
  private selectedShop: string

  constructor(
    private loadingService: LoadingSpinnerService,
    private router: Router,
    private shopBusiness: ShopBusiness,
    private titleService: Title,
    private tipoNegocioBusiness: TipoNegocioBusiness,
    private activatedRoute: ActivatedRoute
  ) {

  }

  async ngOnInit() {
    try {
      this.loadingService.show()
      let promises = []
      this.typeId = await new Promise((resolve, reject) => {
        this.activatedRoute.params.pipe(take(1)).subscribe(async params => {
          resolve(params['type']);
        })
      })
      promises.push(this.shopBusiness.getByType(this.typeId).then(shops => {
        this.shops = Object.values(shops)
      }))
      promises.push(this.tipoNegocioBusiness.getById(this.typeId).then(type => {
        this.type = Object.values(type)[0]
      }))
      await Promise.all(promises)
      this.collapseFilter = true
      this.titleService.setTitle(`${this.type.descripcion} con Domicilios en Santa Rosa De Cabal | Riing.com.co`)
      this.loadingService.hide()
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => ShopsComponent`)
    }
  }

  navigateToProducts(id: string, index) {
    try {
      this.selectedShop = id
      if (this.shops[index].visible) {
        this.router.navigateByUrl(`/shop/products/${this.selectedShop}/${this.typeId}`)
      } else {
        this.shopClosedModal.show()
      }
    } catch (error) {
      console.log(`An error ocurred on navigateToProducts => ShopsComponent`)
    }
  }

  navigateToProductsFromModal() {
    try {
      this.router.navigateByUrl(`/shop/products/${this.selectedShop}/${this.typeId}`)
    } catch (error) {
      console.log(`An error ocurred on navigateToProductsFromModal => ShopsComponent`)
    }
  }

  navigateToTypes() {
    try {
      this.router.navigateByUrl(`/shop/types`)
    } catch (error) {
      console.log(`An error ocurred on navigateToTypes => ShopsComponent`)
    }
  }

  getUrl(id) {
    try {
      return `${environment.url}:${environment.port}/shop/getLogo/${id}`
    } catch (error) {
      console.log(`An error ocurred on getUrl => ShopsComponent`)
    }
  }

  setDefaultPic() {
    try {
      return "../../../assets/img/brand/logo.svg";
    } catch (error) {
      console.log(`An error occurred setDefaultPic => ShopsComponent `)
    }
  }

  collapseFilterFunc() {
    try {
      this.collapseFilter = !this.collapseFilter

    } catch (error) {
      console.log(`An error ocurred on collapseFilterFunc => ShopsComponent`)
    }
  }
}
