import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { ShopsTypesComponent } from './shops-types/shops-types.component';
import { ShopsComponent } from './shops/shops.component';
import { ShopsProductsComponent } from './shops-products/shops-products.component';

const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent,
        data: {
            title: 'Inicio'
        }
    },
    {
        path: '',
        redirectTo: 'types',
        pathMatch: 'full'
    },
    {
        path: 'types',
        component: ShopsTypesComponent,
        data: {
            title: 'Tipos'
        }
    },
    {
        path: 'types/:type',
        component: ShopsComponent,
        data: {
            title: 'Negocios'
        }
    },
    {
        path: 'products/:shop/:shopType',
        component: ShopsProductsComponent,
        data: {
            title: 'Productos'
        }
    },
    {
        path: 'products/:shop/:shopType/t',
        component: ShopsProductsComponent,
        data: {
            title: 'Productos'
        }
    },

];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class ShopRoutingModule { }
