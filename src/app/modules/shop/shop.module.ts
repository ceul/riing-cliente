import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopsTypesComponent } from './shops-types/shops-types.component';
import { ShopsComponent } from './shops/shops.component';
import { ShopsProductsComponent } from './shops-products/shops-products.component';
import { HomeComponent } from './home/home.component';
import { ShopRoutingModule } from './shop-routing.module';
import { FormsModule } from '@angular/forms';
import { LoadingSpinnerComponent } from '../../components/loading-spinner/loading-spinner.component';
import { ComponentsModule } from '../../components/components.module';
import { ProductDetailComponent } from '../../components/product-detail/product-detail.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NewsModalComponent } from '../../components/news-modal/news-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    ShopRoutingModule,
    FormsModule,
    ComponentsModule,
    ModalModule.forRoot(),
    NgSelectModule
  ],
  declarations: [
    ShopsTypesComponent, 
    ShopsComponent, 
    ShopsProductsComponent, 
    HomeComponent,
    
  ],
  bootstrap: [
    LoadingSpinnerComponent,
    ProductDetailComponent,
    NewsModalComponent
  ]
})
export class ShopModule { }
