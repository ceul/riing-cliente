import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TarifasBusiness } from '../../../business/tarifas/tarifas.business';
import TarifaModel from '../../../models/tarifa.model';
import { LoadingSpinnerService } from '../../../services/loading-spinner/loading-spinner.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NewsModalComponent } from '../../../components/news-modal/news-modal.component';

@Component({
  selector: 'riing-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public tarifaSelectedId: string
  public tarifas: TarifaModel[]
  public tarifa: TarifaModel
  public submit: boolean
  constructor(
    private router: Router,
    private tarifaBusiness: TarifasBusiness,
    private loadingSpinnerService: LoadingSpinnerService,
    private modalService: BsModalService,
  ) { }

  async ngOnInit() {
    try {
      this.loadingSpinnerService.show()
      this.tarifas = []
      this.tarifas = Object.values(await this.tarifaBusiness.get())
      this.submit = false
      this.tarifaSelectedId = null
      this.getTarifa()
      this.loadingSpinnerService.hide()
      /*this.modalService.show(NewsModalComponent, {
        class: 'modal-lg modal-dialog-centered',
        animated: true
      })*/
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => HomeComponent`)
    }
  }

  navigateToTypes() {
    try {
      if (this.tarifaSelectedId !== null) {
        localStorage.setItem('tarifa', JSON.stringify(this.tarifa))
        this.router.navigate(['/shop/types'])
      }
      this.submit = true
    } catch (error) {
      console.log(`An error occurred on navigateToTypes => HomeComponent`)
    }
  }

  public tarifaChange() {
    try {
      if (this.tarifaSelectedId !== null) {
        this.tarifa = this.tarifas.find(tarifa => tarifa.id == this.tarifaSelectedId)
      }
    } catch (erorr) {
      console.log(`An error ocurred on tarifaChange => OrderSideDescriptionComponent`)
    }
  }

  getTarifa() {
    try {
      let tarifa = <TarifaModel>JSON.parse(localStorage.getItem('tarifa'))
      if (tarifa !== null && tarifa !== undefined) {
        this.tarifa = tarifa
        this.tarifaSelectedId = this.tarifa.id
      }
    } catch (erorr) {
      console.log(`An error ocurred on getTarifa => OrderSideDescriptionComponent`)
    }
  }
}
