import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LoadingSpinnerService } from '../../../services/loading-spinner/loading-spinner.service';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import UsuarioModel from '../../../models/usuario.model';
import { ToastService } from '../../../services/base-services/toast.service';
import { AuthBusiness } from '../../../business/user/auth.business';
import { SocketService } from '../../../services/socket/socket.service';
import { ResetPasswordModalComponent } from '../reset-password-modal/reset-password-modal.component';
import { RegisterComponent } from '../register/register.component';
import { take } from 'rxjs/operators';

@Component({
  selector: 'riing-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Output() public isLogged: EventEmitter<boolean> = new EventEmitter(); 
  usuario: UsuarioModel;
  loginForm: FormGroup
  public submit: boolean
  public loginError: boolean
  private resetPasswordModal: BsModalRef

  constructor(
    public loginModal: BsModalRef,
    private loadingService: LoadingSpinnerService,
    private authBusiness: AuthBusiness,
    private toast: ToastService,
    private socketService : SocketService,
    private modalService: BsModalService,
  ) {
    this.loginForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'contrasena': new FormControl('', Validators.required),
    })
  }

  ngOnInit() {
    this.submit = false
    this.loginError = false
    this.usuario = new UsuarioModel()
  }

  async onSubmit() {
    try {
      if (this.loginForm.valid) {
        this.loginError = true
        this.loadingService.show()
        this.usuario.email = this.loginForm.controls['email'].value
        this.usuario.contrasena = this.loginForm.controls['contrasena'].value
        let value = await this.authBusiness.val(this.usuario)
        if (value == null) {
          this.loadingService.hide()
          this.loginError = true
          this.toast.showMessage('Usuario o contraseña invalida!!', 'success');
        } else {
          this.loginModal.hide()
          this.loadingService.hide()
          this.loginError = false
          this.socketService.autenticateUser()
          this.isLogged.emit(true)
          this.toast.showMessage('Riing!!', 'success');
        }
      }
      this.submit = true
    } catch (error) {
      this.loadingService.hide()
      this.loginError = true
      this.toast.showMessage('Usuario o contraseña invalida!!', 'error');
    }
  }

  openResetPasswordModal() {
    try {
      this.loginModal.hide()
      this.resetPasswordModal = this.modalService.show(ResetPasswordModalComponent, { class: 'modal-lg modal-dialog-centered', animated: true })
    } catch (error) {
      console.log(`An error ocurred on openResetPasswordModal => LoginComponent`)
    }
  }

  openRegisterModal() {
    try {
      this.loginModal.hide()
      this.resetPasswordModal = this.modalService.show(RegisterComponent, { class: 'modal-lg modal-dialog-centered', animated: true })
      this.resetPasswordModal.content.correctRegister.pipe(take(1)).subscribe(correctRegister => {
        if (correctRegister) {
          this.isLogged.emit(true)
        }
      })
    } catch (error) {
      console.log(`An error ocurred on openRegisterModal => LoginComponent`)
    }
  }

}
