import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { UserBusiness } from '../../../business/user/user.business';
import UsuarioModel from '../../../models/usuario.model';
import TipoUsuarioModel from '../../../models/tipo-usuario.model';
import { LoadingSpinnerService } from '../../../services/loading-spinner/loading-spinner.service';
import { SocketService } from '../../../services/socket/socket.service';
import { AuthBusiness } from '../../../business/user/auth.business';
import { ToastService } from '../../../services/base-services/toast.service';

@Component({
  selector: 'riing-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @Output() public correctRegister: EventEmitter<boolean> = new EventEmitter();

  public registerForm: FormGroup
  public submit: boolean
  private user: UsuarioModel
  public isRegistered: boolean
   

  constructor(
    public registerUserModal: BsModalRef,
    private userBusiness: UserBusiness,
    private loadingService: LoadingSpinnerService,
    private authBusiness: AuthBusiness,
    private socketService: SocketService,
    private toast: ToastService
  ) {
    this.registerForm = new FormGroup({
      'nombre': new FormControl('', Validators.required),
      'apellido': new FormControl('', Validators.required),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'tel1': new FormControl('', [Validators.required, Validators.pattern("[0-9]{7,10}")]),
      'tel2': new FormControl('', Validators.pattern("[0-9]{7,10}")),
      'contrasena': new FormControl('', Validators.required),
      'confirmContrasena': new FormControl('', Validators.required),
      'termsAndConditions': new FormControl(false)
    })
  }

  ngOnInit() {
    try {
      this.submit = false
      this.isRegistered = true
      this.user = new UsuarioModel()
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => RegisterComponent`)
    }
  }

  async register() {
    try {
      if (this.registerForm.controls['termsAndConditions'].value && this.registerForm.valid) {
        if (this.registerForm.controls['contrasena'].value === this.registerForm.controls['confirmContrasena'].value) {
          this.user.email = this.registerForm.controls['email'].value
          this.user.nombre = this.registerForm.controls['nombre'].value
          this.user.apellido = this.registerForm.controls['apellido'].value
          this.user.tel1 = this.registerForm.controls['tel1'].value
          this.user.tel2 = this.registerForm.controls['tel2'].value
          this.user.contrasena = this.registerForm.controls['contrasena'].value
          this.user.tipo = new TipoUsuarioModel()
          this.user.tipo.id = '1'
          this.loadingService.show()
          this.isRegistered = false
          let user = await this.userBusiness.save(this.user)
          this.isRegistered = true
          if (user !== null) {
            let value = await this.authBusiness.val(this.user)
            if (value != null) {
              this.socketService.autenticateUser()
              this.correctRegister.emit(true)
              this.toast.showMessage('Riing!!', 'success');
            }
            this.registerUserModal.hide()
            this.loadingService.hide()
          }
        }
      }
      this.submit = true
    } catch (error) {
      this.loadingService.hide()
      console.log(`An error ocurred on register => RegisterComponent`)
    }
  }

}
