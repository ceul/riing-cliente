import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const routes: Routes = [
    {
        path: 'reset-password/:id',
        component: ResetPasswordComponent,
        data: {
            title: 'Inicio'
        }
    },
    
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class UserRoutingModule { }
