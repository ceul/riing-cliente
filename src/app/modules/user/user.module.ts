import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ComponentsModule } from '../../components/components.module';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResetPasswordModalComponent } from './reset-password-modal/reset-password-modal.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UserRoutingModule } from './user-routing.module';


@NgModule({
  declarations: [
    RegisterComponent, 
    LoginComponent, 
    ResetPasswordModalComponent, 
    ResetPasswordComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    ModalModule.forRoot() ,
    FormsModule,
    ReactiveFormsModule,
    UserRoutingModule
  ],
  exports: [
    RegisterComponent, 
    LoginComponent,
    ResetPasswordModalComponent,
    ResetPasswordComponent
  ],
  providers: [
    BsModalService
  ],
  bootstrap: [
    ResetPasswordModalComponent
  ]
})
export class UserModule { }
