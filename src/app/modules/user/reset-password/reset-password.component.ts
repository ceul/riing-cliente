import { Component, OnInit } from '@angular/core';
import { LoadingSpinnerService } from '../../../services/loading-spinner/loading-spinner.service';
import { UserBusiness } from '../../../business/user/user.business';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'riing-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  private id: string
  public isRestartable: boolean
  public resetPasswordForm: FormGroup
  public submit: boolean
  constructor(
    private loadingService: LoadingSpinnerService,
    private userBusiness: UserBusiness,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { 
    this.resetPasswordForm = new FormGroup({
      'contrasena': new FormControl('', Validators.required),
      'confirmContrasena': new FormControl('', Validators.required)
    })
  }

  async ngOnInit() {
    try{
      this.loadingService.show()
      this.submit = false
      await new Promise((resolve, reject) => {
        this.activatedRoute.params.pipe(take(1)).subscribe(async params => {
          this.id = params['id'];
          resolve()
        })
      })      
      this.isRestartable = <boolean> await this.userBusiness.isChangePasswordId(this.id);
      this.loadingService.hide()
    } catch(error){
      console.log(`An error ocurred on ngOnInit => ResetPasswordComponent`)
    }
  }

  async resetPassword() {
    try {
      if (this.resetPasswordForm.valid) {
        if (this.resetPasswordForm.controls['contrasena'].value === this.resetPasswordForm.controls['confirmContrasena'].value) {
          this.loadingService.show()
          let user = <boolean> await this.userBusiness.changePassword(this.id,this.resetPasswordForm.controls['contrasena'].value )
          if (user == true) {
            this.router.navigateByUrl('/shop')
            this.loadingService.hide()
          }
        }
      }
      this.submit = true
    } catch (error) {
      this.loadingService.hide()
      console.log(`An error ocurred on resetPassword => ResetPasswordComponent`)
    }
  }

}
