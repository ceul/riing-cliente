import { Component, OnInit } from '@angular/core';
//import { LoginComponent } from '../login/login.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { LoadingSpinnerService } from '../../../services/loading-spinner/loading-spinner.service';
import { UserBusiness } from '../../../business/user/user.business';

@Component({
  selector: 'riing-reset-password-modal',
  templateUrl: './reset-password-modal.component.html',
  styleUrls: ['./reset-password-modal.component.scss']
})
export class ResetPasswordModalComponent implements OnInit {
  private loginModal: BsModalRef
  public resetPasswordForm: FormGroup
  public submit: boolean
  public resetError: boolean

  constructor(
    private loadingService: LoadingSpinnerService,
    public resetPasswordModal: BsModalRef,
    private modalService: BsModalService,
    private userBusiness: UserBusiness
  ) {
    this.resetPasswordForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
    })
  }

  ngOnInit() {
    try {
    this.submit = false
    this.resetError = false
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => ResetPasswordComponent`)
    }
  }

  async onSubmit() { 
    try {
      if (this.resetPasswordForm.valid) {
        this.resetError = true
        this.loadingService.show()
        let value = await this.userBusiness.resetPassword(this.resetPasswordForm.controls['email'].value)
        if (value == null) {
          this.loadingService.hide()
          this.resetError = true
        } else {
          this.loadingService.hide()
          this.resetError = false
        }
      }
      this.submit = true
    } catch (error) {
      console.log(`An error ocurred on onSubmit => ResetPasswordComponent`)
    }
  }

}
