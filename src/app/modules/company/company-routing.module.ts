import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContactComponent } from './contact/contact.component';
import { ShopInfoComponent } from './shop-info/shop-info.component';
import { DeliveryInfoComponent } from './delivery-info/delivery-info.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';

const routes: Routes = [
    {
        path: 'contact',
        component: ContactComponent,
        data: {
            title: 'Contactanos'
        }
    },
    {
        path: 'shops-info',
        component: ShopInfoComponent,
        data: {
            title: 'Información para almacenes'
        }
    },
    {
        path: 'delivery-info',
        component: DeliveryInfoComponent,
        data: {
            title: 'Información para domiciliarios'
        }
    },
    {
        path: 'terms-and-conditions',
        component: TermsAndConditionsComponent,
        data: {
            title: 'Términos y condiciones'
        }
    }

    

];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class CompanyRoutingModule { }