import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact/contact.component';
import { CompanyRoutingModule } from './company-routing.module';
import { ShopInfoComponent } from './shop-info/shop-info.component';
import { DeliveryInfoComponent } from './delivery-info/delivery-info.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';

@NgModule({
  declarations: [
    ContactComponent,
    ShopInfoComponent,
    DeliveryInfoComponent,
    TermsAndConditionsComponent
  ],
  imports: [
    CommonModule,
    CompanyRoutingModule
  ]
})
export class CompanyModule { }
