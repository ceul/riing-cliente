import { Component, OnInit, OnDestroy } from '@angular/core';
import { OrdenBusiness } from '../../../business/orden/order.business';
import { AuthBusiness } from '../../../business/user/auth.business';
import { LoadingSpinnerService } from '../../../services/loading-spinner/loading-spinner.service';
import UsuarioModel from '../../../models/usuario.model';
import OrdenModel from '../../../models/orden.model';
import { environment } from '../../../../environments/environment';
import { Subscription } from 'rxjs';
import { SocketService } from '../../../services/socket/socket.service';
import OrderStateEnum from '../../../models/order-state.enum';

@Component({
  selector: 'riing-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit,OnDestroy {

  private user: UsuarioModel
  public orders: any[] 
  private subscription: Subscription

  constructor(private orderBusiness: OrdenBusiness,
    private authBusiness: AuthBusiness,
    private loadingSpinnerService: LoadingSpinnerService,
    private socketService: SocketService) {

  }

  async ngOnInit() {
    try {
      this.loadingSpinnerService.show()
      this.user = this.authBusiness.readUser()
      this.orders = Object.values(await this.orderBusiness.getByClient(this.user.id))
      this.loadingSpinnerService.hide()
      this.subscription = this.socketService.listen('order').subscribe((newOrder)=> {
        let order = <any> newOrder
        let index = this.orders.findIndex(item => item.id === order.id)
        if (index === -1) {
          this.orders.push(order)
        } else {
          this.orders[index].estado = newOrder['estado']
        }
        
      })
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => OrderListComponent`)
    }
  }

  async ngOnDestroy() {
    try {
      this.subscription.unsubscribe()
    } catch (error) {
      console.log(`An error ocurred on ngOnDestroy => OrderListComponent`)
    }
  }

  public getUrl(id) {
    try {
      return `${environment.url}:${environment.port}/shop/getLogo/${id}`
    } catch (error) {
      console.log(`An error ocurred on getUrl => OrderListComponent`)
    }
  }

  getTotal(i) {
    try {
      let total = this.calculateTotal(i, 0, 0, 0)
      return total + this.orders[i].valor
    } catch (error) {
      console.log(`An error ocurred on getTotal => OrderListComponent`)
    }
  }

  calculateTotal(orderIndex, productIndex, itemIndex, total) {
    if (productIndex < this.orders[orderIndex].productos.length) {
      if (itemIndex < this.orders[orderIndex].productos[productIndex].items.length) {
        if (this.orders[orderIndex].productos[productIndex].items[itemIndex].seCobra) {
          let precioItem = this.orders[orderIndex].productos[productIndex].items[itemIndex].precio * this.orders[orderIndex].productos[productIndex].qty
          total = total + precioItem
        }
        return this.calculateTotal(orderIndex, productIndex, itemIndex + 1, total)
      } else {
        total = total + (this.orders[orderIndex].productos[productIndex].producto.precio_venta * this.orders[orderIndex].productos[productIndex].qty)
        return this.calculateTotal(orderIndex, productIndex + 1, 0, total)
      }
    } else {
      return total
    }
  }

}
