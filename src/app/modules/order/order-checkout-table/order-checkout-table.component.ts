import { Component, OnInit } from '@angular/core';
import RelProductoOrdenModel from '../../../models/rel_producto_orden.model';
import UbicacionModel from '../../../models/ubicacion.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import TarifaModel from '../../../models/tarifa.model';
import UsuarioModel from '../../../models/usuario.model';
import OrdenModel from '../../../models/orden.model';
import { Router, ActivatedRoute } from '@angular/router';
import { UbicacionesBusiness } from '../../../business/ubicaciones/ubicaciones.business';
import { LoadingSpinnerService } from '../../../services/loading-spinner/loading-spinner.service';
import { OrdenBusiness } from '../../../business/orden/order.business';
import { AuthBusiness } from '../../../business/user/auth.business';
import { take } from 'rxjs/operators';
import ShoppingCartModel from '../../../models/shopping-cart.model';
import TipoEnvioEnum from '../../../models/tipo_envio.enum';

@Component({
  selector: 'riing-order-checkout-table',
  templateUrl: './order-checkout-table.component.html',
  styleUrls: ['./order-checkout-table.component.scss']
})
export class OrderCheckoutTableComponent implements OnInit {

  public shopId: string
  private shopType: string
  public products: RelProductoOrdenModel[]
  public total: number
  public deliveryWay: string
  public orderStep: number
  public orderStepChange
  public ubicaciones: UbicacionModel[]
  public deliveryDirection: UbicacionModel
  public deliveryForm: FormGroup
  public deliveryFormSubmit: boolean
  public tarifas: TarifaModel[]
  private user: UsuarioModel
  private newOrder: OrdenModel
  public tarifa: TarifaModel
  public onTable: boolean

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private loadingSpinnerService: LoadingSpinnerService,
    private ubicacionesBusiness: UbicacionesBusiness,
    private ordenBusiness: OrdenBusiness,
    private ubicacionBusiness: UbicacionesBusiness,
    private authBusiness: AuthBusiness
  ) {
    this.deliveryForm = new FormGroup({
      'id': new FormControl('null'),
      'indicaciones': new FormControl('', Validators.required),
    })
  }

  async ngOnInit() {
    try {
      this.loadingSpinnerService.show()
      localStorage.setItem('deliveryWay', TipoEnvioEnum.ON_TABLE.toString())
      this.orderStep = 1
      this.onTable = true
      this.newOrder = new OrdenModel()
      this.deliveryDirection = new UbicacionModel()
      this.ubicaciones = []
      this.tarifas = []
      let tarifa = JSON.parse(localStorage.getItem('tarifa'))
      if (tarifa !== null && tarifa !== undefined && tarifa !== '') {
        this.tarifa = tarifa
      } else {
        this.tarifa = new TarifaModel()
      }
      this.deliveryFormSubmit = false
      let promises = []
      promises.push(new Promise((resolve, reject) => {
        this.activatedRoute.params.pipe(take(1)).subscribe(async params => {
          this.shopId = params['shop'];
          this.shopType = params['shopType']
          this.ubicaciones = Object.values(await this.ubicacionesBusiness.getByUser(this.shopId))
          resolve()
        })
      }))

      this.user = <UsuarioModel>this.authBusiness.readUser()
      this.newOrder.cliente = this.user.id

      

      this.deliveryDirection.usuario = this.user.id

      await Promise.all(promises)

      this.getOrder()
      //this.calculateTotal()
      this.loadingSpinnerService.hide()
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => OrderCheckoutComponent`)
    }
  }

  returnToShop() {
    try {
      this.router.navigateByUrl(`/shop/products/${this.shopId}/${this.shopType}/t`)
    } catch (erorr) {
      console.log(`An error ocurred on returnToShop => OrderCheckoutComponent`)
    }
  }

  getOrder() {
    try {
      let shoppingCart = <ShoppingCartModel[]>JSON.parse(localStorage.getItem('shoppingCart'))
      if (shoppingCart !== null && shoppingCart !== undefined && shoppingCart.length > 0) {
        let index = shoppingCart.findIndex(shop => shop.almacen === this.shopId)
        if (index !== -1) {
          this.products = shoppingCart[index].orden
          this.newOrder.productos = this.products
        }
      } else {
        this.router.navigateByUrl('/shop');
      }
    } catch (erorr) {
      console.log(`An error ocurred on getOrder => OrderCheckoutComponent`)
    }
  }

  calculateTotal() {
    try {
      this.total = 0
      this.products.forEach(product => {
        this.total = this.total + product.total
      })
      //if (this.deliveryWay == TipoEnvioEnum.DELIVERY.toString()) {
        let tarifa = <TarifaModel>JSON.parse(localStorage.getItem('tarifa'))
        this.total = this.total + tarifa.valor
      //}
    } catch (erorr) {
      console.log(`An error ocurred on calculateTotal => OrderCheckoutComponent`)
    }
  }


  forwardBtnClick() {
    try {
      this.orderStep = this.orderStep - 1
    } catch (erorr) {
      console.log(`An error ocurred on forwardBtnClick => OrderCheckoutComponent`)
    }
  }

  async nextBtnClick() {
    try {
      if (this.orderStep == 1 && this.deliveryForm.valid && this.deliveryForm.controls['indicaciones'].value !== 'null') {
        await this.finishOrder()
        this.orderStep = 3
      }
      this.deliveryFormSubmit = true
    } catch (erorr) {
      console.log(`An error ocurred on nextBtnClick => OrderCheckoutComponent`)
    }
  }

  async finishOrder() {
    try {
      this.loadingSpinnerService.show()
      this.newOrder.hora_inicio = new Date()
      if(this.deliveryDirection.id === 'null'){
        this.deliveryDirection.id = ''
      }
      this.newOrder.ubicacion = this.ubicaciones[0]
      this.newOrder.tipo_pago = '1'
      this.newOrder.tipo_envio = TipoEnvioEnum.ON_TABLE.toString()
      await this.ordenBusiness.save(this.newOrder)
      let shoppingCart = <ShoppingCartModel[]>JSON.parse(localStorage.getItem('shoppingCart'))
      shoppingCart = shoppingCart.filter(shop => shop.almacen !== this.shopId)
      localStorage.setItem('shoppingCart', JSON.stringify(shoppingCart))
      this.loadingSpinnerService.hide()
    } catch (erorr) {
      console.log(`An error ocurred on finishOrder => OrderCheckoutComponent`)
    }
  }

  deliveryWayChange(event){
    try { 
      this.deliveryWay = event
    } catch (erorr) {
      console.log(`An error ocurred on deliveryWayChange => OrderCheckoutComponent`)
    }
  }
}
