import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCheckoutTableComponent } from './order-checkout-table.component';

describe('OrderCheckoutTableComponent', () => {
  let component: OrderCheckoutTableComponent;
  let fixture: ComponentFixture<OrderCheckoutTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderCheckoutTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCheckoutTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
