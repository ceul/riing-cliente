import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderCheckoutComponent } from './order-checkout/order-checkout.component';
import { OrderRoutingModule } from './order-routing.module';
import { ComponentsModule } from '../../components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '../../pipes/pipes.module';
import { AuthGuardService } from '../../guards/auth-guard.service';
import { OrderCheckoutTableComponent } from './order-checkout-table/order-checkout-table.component';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
    OrderListComponent,
    OrderCheckoutComponent,
    OrderCheckoutTableComponent
  ],
  imports: [
    CommonModule,
    OrderRoutingModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule.forRoot(),
    NgSelectModule
  ],
  providers: [
    AuthGuardService
  ]
})
export class OrderModule { }
