import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import ShoppingCartModel from '../../../models/shopping-cart.model';
import RelProductoOrdenModel from '../../../models/rel_producto_orden.model';
import { LoadingSpinnerService } from '../../../services/loading-spinner/loading-spinner.service';
import { UbicacionesBusiness } from '../../../business/ubicaciones/ubicaciones.business';
import UbicacionModel from '../../../models/ubicacion.model';
import { AuthBusiness } from '../../../business/user/auth.business';
import UsuarioModel from '../../../models/usuario.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import TarifaModel from '../../../models/tarifa.model';
import { TarifasBusiness } from '../../../business/tarifas/tarifas.business';
import OrdenModel from '../../../models/orden.model';
import { OrdenBusiness } from '../../../business/orden/order.business';
import TipoEnvioEnum from '../../../models/tipo_envio.enum';

@Component({
  selector: 'riing-order-checkout',
  templateUrl: './order-checkout.component.html',
  styleUrls: ['./order-checkout.component.scss']
})
export class OrderCheckoutComponent implements OnInit {

  public shopId: string
  private shopType: string
  public products: RelProductoOrdenModel[]
  public total: number
  public deliveryWay: string
  public orderStep: number
  public orderStepChange
  public ubicaciones: UbicacionModel[]
  public deliveryDirection: UbicacionModel
  public deliveryForm: FormGroup
  public deliveryFormSubmit: boolean
  public tarifas: TarifaModel[]
  private user: UsuarioModel
  private newOrder: OrdenModel
  public tarifa: TarifaModel

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private loadingSpinnerService: LoadingSpinnerService,
    private ubicacionesBusiness: UbicacionesBusiness,
    private ordenBusiness: OrdenBusiness,
    private authBusiness: AuthBusiness,
    private tarifaBusiness: TarifasBusiness
  ) {
    this.deliveryForm = new FormGroup({
      'id': new FormControl('null'),
      'type': new FormControl('Carrera', Validators.required),
      'street': new FormControl('', Validators.required),
      'nombre': new FormControl('', Validators.required),
      //'corner': new FormControl('', Validators.required),
      //'number': new FormControl('', Validators.required),
      'barrio': new FormControl(null, Validators.required),
      'indicaciones': new FormControl(''),
    })
  }

  async ngOnInit() {
    try {
      this.loadingSpinnerService.show()
      this.orderStep = 1
      this.newOrder = new OrdenModel()
      this.deliveryDirection = new UbicacionModel()
      this.ubicaciones = []
      this.tarifas = []
      let tarifa = JSON.parse(localStorage.getItem('tarifa'))
      if (tarifa !== null && tarifa !== undefined && tarifa !== '') {
        this.tarifa = tarifa
      } else {
        this.tarifa = new TarifaModel()
      }
      this.deliveryFormSubmit = false
      let promises = []
      promises.push(new Promise((resolve, reject) => {
        this.activatedRoute.params.pipe(take(1)).subscribe(async params => {
          this.shopId = params['shop'];
          this.shopType = params['shopType']
          resolve()
        })
      }))

      this.user = <UsuarioModel>this.authBusiness.readUser()
      this.newOrder.cliente = this.user.id

      promises.push(this.ubicacionesBusiness.getByUser(this.user.id).then(ubicaciones => {
        this.ubicaciones = Object.values(ubicaciones)
      }))

      this.deliveryDirection.usuario = this.user.id

      promises.push(this.tarifaBusiness.get().then(tarifas => {
        this.tarifas = Object.values(tarifas)
      }))

      await Promise.all(promises)

      this.getOrder()
      //this.calculateTotal()
      this.loadingSpinnerService.hide()
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => OrderCheckoutComponent`)
    }
  }

  returnToShop() {
    try {
      this.router.navigateByUrl(`/shop/products/${this.shopId}/${this.shopType}`)
    } catch (erorr) {
      console.log(`An error ocurred on returnToShop => OrderCheckoutComponent`)
    }
  }

  getOrder() {
    try {
      let shoppingCart = <ShoppingCartModel[]>JSON.parse(localStorage.getItem('shoppingCart'))
      if (shoppingCart !== null && shoppingCart !== undefined && shoppingCart.length > 0) {
        let index = shoppingCart.findIndex(shop => shop.almacen === this.shopId)
        if (index !== -1) {
          this.products = shoppingCart[index].orden
          this.newOrder.productos = this.products
        }
      } else {
        this.router.navigateByUrl('/shop');
      }
    } catch (erorr) {
      console.log(`An error ocurred on getOrder => OrderCheckoutComponent`)
    }
  }

  calculateTotal() {
    try {
      this.total = 0
      this.products.forEach(product => {
        this.total = this.total + product.total
      })
      //if (this.deliveryWay == TipoEnvioEnum.DELIVERY.toString()) {
      this.getRate()
      this.total = this.total + this.tarifa.valor
      //}
    } catch (erorr) {
      console.log(`An error ocurred on calculateTotal => OrderCheckoutComponent`)
    }
  }


  forwardBtnClick() {
    try {
      this.orderStep = this.orderStep - 1
    } catch (erorr) {
      console.log(`An error ocurred on forwardBtnClick => OrderCheckoutComponent`)
    }
  }

  async nextBtnClick() {
    try {
      if (this.orderStep == 1 && this.deliveryForm.valid && this.deliveryForm.controls['barrio'].value !== null) {
        this.orderStep = this.orderStep + 1
      } else if (this.orderStep == 2) {

        await this.finishOrder()
        this.orderStep = this.orderStep + 1
      }
      this.deliveryFormSubmit = true
    } catch (erorr) {
      console.log(`An error ocurred on nextBtnClick => OrderCheckoutComponent`)
    }
  }

  async getUbication() {
    try {
      this.deliveryDirection = new UbicacionModel()
      this.deliveryDirection.id = this.deliveryForm.controls['id'].value
      this.deliveryDirection.indicaciones = this.deliveryForm.controls['indicaciones'].value
      this.deliveryDirection.nombre = this.deliveryForm.controls['nombre'].value
      this.deliveryDirection.tarifa = this.deliveryForm.controls['barrio'].value
      this.deliveryDirection.usuario = this.user.id
      this.deliveryDirection.direccion = `${this.deliveryForm.controls['type'].value} ${this.deliveryForm.controls['street'].value}`
    } catch (erorr) {
      console.log(`An error ocurred on saveUbication => OrderCheckoutComponent`)
    }
  }


  selectedUbication() {
    try {
      if (this.deliveryForm.controls['id'].value == "null") {
        this.deliveryForm.controls['type'].setValue('Carrera')
        this.deliveryForm.controls['street'].setValue('')
        /*this.deliveryForm.controls['corner'].setValue('')
        this.deliveryForm.controls['number'].setValue('')*/
        this.deliveryForm.controls['barrio'].setValue(null)
        this.deliveryForm.controls['nombre'].setValue('')
      } else {
        let direction = this.ubicaciones.find(ubicacion => ubicacion.id === this.deliveryForm.controls['id'].value)
        let typeIndex = direction.direccion.indexOf(' ')
        this.deliveryForm.controls['type'].setValue(direction.direccion.slice(0, typeIndex))
        //let streetIndex = direction.direccion.indexOf('#', typeIndex)
        this.deliveryForm.controls['street'].setValue(direction.direccion.slice(typeIndex + 1))// , streetIndex))
        //let cornerIndex = direction.direccion.indexOf('-', streetIndex)
        //this.deliveryForm.controls['corner'].setValue(direction.direccion.slice(streetIndex + 1, cornerIndex))
        //this.deliveryForm.controls['number'].setValue(direction.direccion.slice(cornerIndex + 1))
        this.deliveryForm.controls['barrio'].setValue(direction.tarifa)
        this.deliveryForm.controls['nombre'].setValue(direction.nombre)
        this.getRate()
        
      }
    } catch (erorr) {
      console.log(`An error ocurred on selectedUbication => OrderCheckoutComponent`)
    }
  }

  getRate() {
    try {
      this.tarifa = this.tarifas.find(tarifa => tarifa.id ==  this.deliveryForm.controls['barrio'].value)
    } catch (erorr) {
      console.log(`An error ocurred on getRate => OrderCheckoutComponent`)
    }
  }

  async finishOrder() {
    try {
      this.loadingSpinnerService.show()
      this.calculateTotal()
      this.newOrder.hora_inicio = new Date()
      this.getUbication()
      if (this.deliveryDirection.id === 'null') {
        this.deliveryDirection.id = ''
      }
      this.newOrder.ubicacion = this.deliveryDirection
      this.newOrder.tipo_pago = '1'
      this.newOrder.tipo_envio = localStorage.getItem('deliveryWay')
      await this.ordenBusiness.save(this.newOrder)
      let shoppingCart = <ShoppingCartModel[]>JSON.parse(localStorage.getItem('shoppingCart'))
      shoppingCart = shoppingCart.filter(shop => shop.almacen !== this.shopId)
      localStorage.setItem('shoppingCart', JSON.stringify(shoppingCart))
      this.loadingSpinnerService.hide()
    } catch (erorr) {
      console.log(`An error ocurred on finishOrder => OrderCheckoutComponent`)
    }
  }

  deliveryWayChange(event) {
    try {
      this.deliveryWay = event
    } catch (erorr) {
      console.log(`An error ocurred on deliveryWayChange => OrderCheckoutComponent`)
    }
  }
}
