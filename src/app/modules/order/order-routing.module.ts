import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { OrderCheckoutComponent } from './order-checkout/order-checkout.component';
import { OrderListComponent } from './order-list/order-list.component';
import { AuthGuardService } from '../../guards/auth-guard.service';
import { OrderCheckoutTableComponent } from './order-checkout-table/order-checkout-table.component';

const routes: Routes = [
    {
        path: ':shop/:shopType',
        canActivate: [AuthGuardService ],
        component: OrderCheckoutComponent,
        data: {
            title: 'Pago de Orden'
        }
    },
    {
        path: ':shop/:shopType/t',
        canActivate: [AuthGuardService ],
        component: OrderCheckoutTableComponent,
        data: {
            title: 'Pago de Orden'
        }
    },
    {
        path: 'list',
        component: OrderListComponent,
        canActivate: [AuthGuardService],
        data: {
            title: 'Lista de ordenes'
        }
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class OrderRoutingModule { }
