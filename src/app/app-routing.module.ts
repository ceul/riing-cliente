import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules, ExtraOptions } from '@angular/router';
import { LayoutComponent } from './layout/layout/layout.component';
const routerOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  scrollOffset: [0, 64],
  preloadingStrategy: PreloadAllModules,
  useHash: true 
};
const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'shop',
        pathMatch: 'full'
      },
      {
        path: 'shop',
        loadChildren: './modules/shop/shop.module#ShopModule'
      },
      {
        path: 'user',
        loadChildren: './modules/user/user.module#UserModule'
      },
      {
        path: 'order',
        loadChildren: './modules/order/order.module#OrderModule'
      },
      {
        path: 'company',
        loadChildren: './modules/company/company.module#CompanyModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
