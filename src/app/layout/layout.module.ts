import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LayoutComponent } from './layout/layout.component';
import { SubHeaderComponent } from './sub-header/sub-header.component';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../../app/components/components.module';
import { BsModalService } from 'ngx-bootstrap/modal';
import { RegisterComponent } from '../modules/user/register/register.component';
import { UserModule } from '../modules/user/user.module';
import { LoginComponent } from '../modules/user/login/login.component';

@NgModule({
  declarations: [
    HeaderComponent, 
    FooterComponent, 
    LayoutComponent, 
    SubHeaderComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ComponentsModule,
    UserModule
  ]
  ,
  providers: [
    BsModalService
  ],
  bootstrap: [
    RegisterComponent,
    LoginComponent
  ]
})
export class LayoutModule { }
