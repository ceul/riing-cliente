import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { RegisterComponent } from '../../modules/user/register/register.component';
import { LoginComponent } from '../../modules/user/login/login.component';
import { AuthService } from '../../services/user/auth.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'riing-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public clickMenu: boolean
  private registerModal: BsModalRef
  private loginModal: BsModalRef


  constructor(private modalService: BsModalService,
    private authService: AuthService) { }

  ngOnInit() {
    try {
      this.clickMenu = false
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => HeaderComponent`)
    }
  }


  openRegisterModal() {
    try {
      this.registerModal = this.modalService.show(RegisterComponent, { class: 'modal-lg modal-dialog-centered', animated: true })
      this.registerModal.content.correctRegister.pipe(take(1)).subscribe(correctRegister => {
        if (correctRegister) {
          this.openLoginModal()
        }
      })
    } catch (error) {
      console.log(`An error ocurred on openRegisterModal => HeaderComponent`)
    }
  }

  openLoginModal() {
    try {
      this.loginModal = this.modalService.show(LoginComponent, { class: 'modal-lg modal-dialog-centered', animated: true })
      this.loginModal.content.isLogged.pipe(take(1)).subscribe(isLogged => {
        if (isLogged) {
          this.authService.isLoggedFunc(isLogged)
        }
      })
    } catch (error) {
      console.log(`An error ocurred on openLoginModal => HeaderComponent`)
    }
  }

}
