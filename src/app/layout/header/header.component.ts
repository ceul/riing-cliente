import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { RegisterComponent } from '../../modules/user/register/register.component';
import { LoginComponent } from '../../modules/user/login/login.component';
import { take } from 'rxjs/operators';
import UsuarioModel from '../../models/usuario.model';
import { AuthBusiness } from '../../business/user/auth.business';
import { AuthService } from '../../services/user/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { SocketService } from '../../services/socket/socket.service';
import OrderStateEnum from '../../models/order-state.enum';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'riing-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  public itsScrolled: boolean
  public clickMenu: boolean
  public user: UsuarioModel
  public clickOnUser: boolean
  public clickOnNotification: boolean

  private registerModal: BsModalRef
  private loginModal: BsModalRef
  private subscription: Subscription
  private showLogin: boolean
  public notificationsNumber: number
  public notifications: any[]

  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;
    if (pos != max) {
      this.itsScrolled = true
    } else {
      this.itsScrolled = false
    }
  }

  constructor(
    private modalService: BsModalService,
    private authBusiness: AuthBusiness,
    private authService: AuthService,
    private socketService: SocketService,
    private titleService: Title,
    private router: Router
  ) { }

  ngOnInit() {
    try {
      this.itsScrolled = false
      this.clickMenu = false
      this.clickOnUser = false
      this.notificationsNumber = 0
      this.titleService.setTitle(`Domicilios en Santa Rosa de Cabal | Riing.com.co`)
      this.notifications = []
      this.clickOnNotification = false
      this.showLogin = false
      if (this.authBusiness.isAuthenticated()) {
        let user = this.authBusiness.readUser()
        if (user !== null && user !== undefined) {
          this.user = user
        }
      }
      /* else {
        if (!this.showLogin) {
          this.onClickMobileMenu()
          this.openLoginModal()
          this.showLogin = true
        }
      }*/

      this.subscription = this.authService.isLogged.subscribe(isLogged => {
        if (isLogged) {
          if (this.authBusiness.isAuthenticated()) {
            let user = this.authBusiness.readUser()
            if (user !== null && user !== undefined) {
              this.user = user
            }
          }
        } else{
          if (!this.showLogin) {
            this.onClickMobileMenu()
            this.openLoginModal()
            this.showLogin = true
          }
        }
      })

      this.subscription.add(this.socketService.listen('order').subscribe(newOrder => {
        let order = <any>newOrder
        let description = ''
        let route = ''
        if (order.estado == OrderStateEnum.PROCESS) {
          description = `Orden en proceso`
          route = '/order/list'
        } else if (order.estado == OrderStateEnum.READY) {
          description = `Orden Lista`
          route = '/order/list'
        } else if (order.estado == OrderStateEnum.DELIVERY_MEN_ASIGNED) {
          description = `Domiciliario Asignado`
          route = '/order/list'
        } else if (order.estado == OrderStateEnum.ON_DELIVERY) {
          description = `Orden Despachada`
          route = '/order/list'
        }
        if (order.estado !== OrderStateEnum.FINISH) {
          this.notificationsNumber++
          let title = this.titleService.getTitle()
          this.titleService.setTitle(`(${this.notificationsNumber}) ${title}`)
          this.notifications.unshift({ description, route })
        }
      }))

    } catch (error) {
      console.log(`An error ocurred on ngOnInit => HeaderComponent`)
    }
  }

  ngOnDestroy() {
    try {
      this.subscription.unsubscribe()
    } catch (error) {
      console.log(`An error ocurred on ngOnDestroy => HeaderComponent`)
    }
  }

  public onClickMobileMenu() {
    try {
      this.clickMenu = this.clickMenu !== true
    } catch (error) {
      console.log(`An error ocurred on onClickMobileMenu => HeaderComponent`)
    }
  }

  openRegisterModal() {
    try {
      this.onClickMobileMenu()
      this.registerModal = this.modalService.show(RegisterComponent, { class: 'modal-lg modal-dialog-centered', animated: true })
      this.registerModal.content.correctRegister.pipe(take(1)).subscribe(correctRegister => {
        if (correctRegister) {
          this.openLoginModal()
        }
      })
    } catch (error) {
      console.log(`An error ocurred on openRegisterModal => HeaderComponent`)
    }
  }

  openLoginModal() {
    try {
      this.onClickMobileMenu()
      this.loginModal = this.modalService.show(LoginComponent, { class: 'modal-lg modal-dialog-centered', animated: true })
      this.loginModal.content.isLogged.pipe(take(1)).subscribe(isLogged => {
        if (isLogged) {
          this.user = this.authBusiness.readUser()
        }
      })
    } catch (error) {
      console.log(`An error ocurred on openLoginModal => HeaderComponent`)
    }
  }

  logout() {
    try {
      this.authBusiness.logout()
      this.user = null
      this.onClickMobileMenu()
      this.router.navigateByUrl('/shop')
    } catch (error) {
      console.log(`An error ocurred on logout => HeaderComponent`)
    }
  }

  notificationsOnClick() {
    try {
      this.clickOnNotification = !this.clickOnNotification
      this.notificationsNumber = 0
      this.titleService.setTitle(`Riing`)
    } catch (error) {
      console.log(`An error occurred on notificationsOnClick => DefaultLayoutComponent`)
    }
  }

  notificationOnClick(notification) {
    try {
      this.notificationsNumber = 0
      this.onClickMobileMenu()
      this.router.navigateByUrl(notification.route);
    } catch (error) {
      console.log(`An error occurred on notificationOnClick => DefaultLayoutComponent`)
    }
  }

}
