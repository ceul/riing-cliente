import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
				path: '',
				redirectTo: 'shop',
				pathMatch: 'full'
			},
			{
				path: 'shop',
				loadChildren: '../modules/shop/shop.module#ShopModule'
			},
        ]
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
