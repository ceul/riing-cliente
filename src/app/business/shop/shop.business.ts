import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import TipoNegocioModel from '../../models/tipo-negocio.model';
import { ShopService } from '../../services/shop/shop.service';

@Injectable({
	providedIn: 'root'
})
export class ShopBusiness {

	className = 'ShopBusiness'

	constructor(private shopService: ShopService) { }

	get() {
		try {
			return this.shopService.get();
		} catch (e) {
			console.log(`An error occurred getting shop ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.shopService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting by id shop ${this.getById.name} --> ${this.className}`)
		}
	}

	getByType(type: string) {
		try {
			return this.shopService.getByType(type);
		} catch (e) {
			console.log(`An error occurred getting by id shop ${this.getByType.name} --> ${this.className}`)
		}
	}

}
