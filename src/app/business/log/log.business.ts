import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LogService } from '../../services/log/log.service';
import LogModel from '../../models/log.model';

@Injectable({
	providedIn: 'root'
})
export class LogBusiness {

	className = 'LogBusiness'

	constructor(private logService: LogService) { }

	save(data: LogModel) {
		try {
			return this.logService.save(data);
		} catch (e) {
			console.log(`An error occurred saving log ${this.save.name} --> ${this.className}`)
		}
	}

	
}
