import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UbicacionesService } from '../../services/ubicaciones/ubicaciones.service';
import UbicacionModel from '../../models/ubicacion.model';

@Injectable({
	providedIn: 'root'
})
export class UbicacionesBusiness {

	className = 'UbicacionesBusiness'

	constructor(private ubicacionService: UbicacionesService) { }

	save(ubicacion: UbicacionModel) {
		try {
			return this.ubicacionService.save(ubicacion);
		} catch (e) {
			console.log(`An error occurred saving ubicacion ${this.save.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.ubicacionService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting ubicacion by id ${this.getById.name} --> ${this.className}`)
		}
	}

	getByUser(id: string) {
		try {
			return this.ubicacionService.getByUser(id);
		} catch (e) {
			console.log(`An error occurred getting ubicacion by user ${this.getByUser.name} --> ${this.className}`)
		}
	}

	update(ubicacion: UbicacionModel) {
		try {
			return this.ubicacionService.update(ubicacion);
		} catch (e) {
			console.log(`An error occurred updating ubicacion ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.ubicacionService.delete(id);
		} catch (e) {
			console.log(`An error occurred deletting ubicacion by id ${this.delete.name} --> ${this.className}`)
		}
	}

}
