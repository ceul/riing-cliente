import { Injectable } from '@angular/core';
import { CategoryService } from '../../services/category/category.service';
import CategoryModel from '../../models/category.model';

@Injectable({
	providedIn: 'root'
})
export class CategoryBusiness {

	className = 'CategoryBusiness'

	constructor(private categoryService: CategoryService) { }

	get() {
		try {
			return this.categoryService.get();
		} catch (e) {
			console.log(`An error occurred getting categories ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.categoryService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting category by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	getWithProductsByShop(almacen: string) {
		try {
			return this.categoryService.getWithProductsByShop(almacen);
		} catch (e) {
			console.log(`An error occurred getting category with products by shop: ${this.getWithProductsByShop.name} --> ${this.className}`)
		}
	}
}
