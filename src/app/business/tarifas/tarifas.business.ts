import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TarifasService } from '../../services/tarifas/tarifas.service';

@Injectable({
	providedIn: 'root'
})
export class TarifasBusiness {

	className = 'TarifasBusiness'

	constructor(private tarifaService: TarifasService) { }



	getById(id: string) {
		try {
			return this.tarifaService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting tarifa by id ${this.getById.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.tarifaService.get();
		} catch (e) {
			console.log(`An error occurred getting tarifas by user ${this.get.name} --> ${this.className}`)
		}
	}

}
