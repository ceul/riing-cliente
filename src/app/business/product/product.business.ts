import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductService } from '../../services/product/product.service';

@Injectable({
	providedIn: 'root'
})
export class ProductBusiness {

	className = 'ProductBusiness'

	constructor(private productService: ProductService) { }

	get() {
		try {
			return this.productService.get();
		} catch (e) {
			console.log(`An error occurred getting products: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.productService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting product by id: ${this.getById.name} --> ${this.className}`)
		}
	}


	getByCategory(categoria: string, almacen: string) {
		try {
			return this.productService.getByCategory(categoria, almacen);
		} catch (e) {
			console.log(`An error occurred getting products by category: ${this.getByCategory.name} --> ${this.className}`)
		}
	}

	getProductByShop(almacen: string) {
		try {
			return this.productService.getProductByShop(almacen);
		} catch (e) {
			console.log(`An error occurred getting products by shop: ${this.getProductByShop.name} --> ${this.className}`)
		}
	}
}