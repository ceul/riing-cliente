import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebPushService } from '../../services/web-push/web-push.service';
import WebPushModel from '../../models/web-push.model';

@Injectable({
	providedIn: 'root'
})
export class WebPushBusiness {

	className = 'WebPushBusiness'

	constructor(private webPushService: WebPushService) { }


	insertSubscription(subscription: any) {
		try {
			return this.webPushService.insertSubscription(subscription);
		} catch (e) {
			console.log(`An error occurred inserting subscription ${this.insertSubscription.name} --> ${this.className}`)
		}
	}
	
	sendNotification(notification: WebPushModel) {
		try {
			return this.webPushService.sendNotification(notification);
		} catch (e) {
			console.log(`An error occurred sending notification ${this.sendNotification.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.webPushService.get();
		} catch (e) {
			console.log(`An error occurred getting notifications ${this.get.name} --> ${this.className}`)
		}
	}

	

}
