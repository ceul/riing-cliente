import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import AlmacenModel from '../../models/almacen.model';
import { AlmacenService } from '../../services/almacen/almacen.service';

@Injectable({
	providedIn: 'root'
})
export class AlmacenBusiness {

	className = 'AlmacenBusiness'

	constructor(private almacenService: AlmacenService) { }

	getById(id: string) {
		try {
			return this.almacenService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving Almacen ${this.getById.name} --> ${this.className}`)
		}
	}

}
