import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TipoNegocioService } from '../../services/tipo-negocio/tipo-negocio.service';
import TipoNegocioModel from '../../models/tipo-negocio.model';

@Injectable({
	providedIn: 'root'
})
export class TipoNegocioBusiness {

	className = 'TipoNegocioBusiness'

	constructor(private tipoNegocioService: TipoNegocioService) { }

	get() {
		try {
			return this.tipoNegocioService.get();
		} catch (e) {
			console.log(`An error occurred getting tipo negocio ${this.get.name} --> ${this.className}`)
		}
	}

	getVisible() {
		try {
			return this.tipoNegocioService.getVisible();
		} catch (e) {
			console.log(`An error occurred getting tipo negocio ${this.getVisible.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.tipoNegocioService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting by id tipo negocio ${this.getById.name} --> ${this.className}`)
		}
	}

}
