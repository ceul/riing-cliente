import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../services/user/auth.service';
import { Router } from '@angular/router';
import UsuarioModel from '../../models/usuario.model';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})

export class AuthBusiness {

  userToken: string = '';
  user: UsuarioModel
  className = 'AuthBusiness';

  constructor(private authService: AuthService,
    private router: Router,
    public jwtHelper: JwtHelperService) { }

  public val(id: UsuarioModel) {
    try {
      let res = this.authService.val(id).then(value => {
        let otra: string = value['tokenReturn'];
        this.userToken = value['tokenReturn'].toString();
        this.saveToken(value['tokenReturn']);
        this.saveUser(value['user'])
        return value;
      }).catch(err => {
        console.log('I did not think that this would happen: ', err);
        return null;
      });

      return res;
    } catch (e) {
      console.log(
        `An error occurred validating user ${this.val.name} --> ${
        this.className
        }`
      );
    }
  }

  logout() {
    try {
      localStorage.clear()
    } catch (e) {
      throw new Error(`An error occurred login out => ${e.message}`)
    }
  }

  private saveToken(idToken: string) {
    try {
      this.userToken = idToken;
      localStorage.setItem('token', idToken)
      let today = new Date();
      today.setSeconds(3600);
      localStorage.setItem('expire', today.getTime().toString())
    } catch (error) {
      console.log('An error occurred saving the token')
    }
  }

  public saveUser(user: UsuarioModel) {
    try {
      this.user = user;
      localStorage.setItem('user', JSON.stringify(user))
    } catch (error) {
      console.log('An error occurred saving the user')
    }
  }

  readUser(): UsuarioModel {
    try {
      let val = JSON.parse(localStorage.getItem('user'))
      if (val) {
        return val;
      } else {
        return null;
      }
    } catch (error) {
      console.log('An error occurred reading the user')
    }
  }

  async readToken(): Promise<string> {
    try {
      let val = localStorage.getItem('token')
      if (val) {
        return val;
      } else {
        return '';
      }
    } catch (error) {
      console.log('An error occurred reading the token')
    }
  }

  async isValid(): Promise<boolean> {
    try {
      let token = await this.readToken()
      if (token.length > 2) {
        return true;
      }
      else {
        return false;
      }
    } catch (error) {
      return false
    }
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    // Check whether the token is expired and return
    // true or false
    return !this.jwtHelper.isTokenExpired(token);
  }
}
