import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../../services/user/user.service';
import UsuarioModel from '../../models/usuario.model';

@Injectable({
	providedIn: 'root'
})
export class UserBusiness {

	className = 'UserBusiness'

	constructor(private userService: UserService) { }

	save(data: UsuarioModel) {
		try {
			return this.userService.save(data);
		} catch (e) {
			console.log(`An error occurred saving user ${this.save.name} --> ${this.className}`)
		}

	}

	update(data: UsuarioModel) {
		try {
			return this.userService.update(data);
		} catch (e) {
			console.log(`An error occurred saving user ${this.save.name} --> ${this.className}`)
		}
	}

	resetPassword(email: string){
		try {
			return this.userService.resetPassword(email);
		} catch (e) {
			console.log(`An error occurred resetting password ${this.resetPassword.name} --> ${this.className}`)
		}
	}

	changePassword(id: string, newPassword: string){
		try {
			return this.userService.changePassword(id, newPassword);
		} catch (e) {
			console.log(`An error occurred resetting password ${this.changePassword.name} --> ${this.className}`)
		}
	}

	isChangePasswordId(id: string){
		try {
			return this.userService.isChangePasswordId(id);
		} catch (e) {
			console.log(`An error occurred resetting password ${this.isChangePasswordId.name} --> ${this.className}`)
		}
	}
}
