import { Component, OnInit, OnDestroy, ApplicationRef } from '@angular/core';
import { setTheme } from 'ngx-bootstrap/utils';
import { SocketService } from './services/socket/socket.service';
import { Subscription, interval } from 'rxjs';
import { SwUpdate, SwPush } from '@angular/service-worker';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { WebPushBusiness } from './business/web-push/web-push.business';

declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'riing-cliente';
  private subscription: Subscription

  private readonly publicKey = "BGUBcvGJzBIqgAvgoNk7Li25Cb0RuIAeHqTM82gs_WxBkXcuo99dSLzkIgeZCs9HfhYHfD8bEVgFxN8VnMQ1YhY"

  constructor(private socketService: SocketService,
    private push: SwPush,
    private appRef: ApplicationRef,
    private update: SwUpdate,
    private webPushBusiness: WebPushBusiness,
    private router: Router) {
    setTheme('bs3');
    const navEndEvents$ = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      );

    navEndEvents$.subscribe((event: NavigationEnd) => {
      gtag('config', 'UA-173674322-1', {
        'page_path': event.urlAfterRedirects
      });
    });
  }

  ngOnInit() {
    try {
      this.socketService.setupSocketConnection();
      this.subscription = this.socketService.listen('reconnect_attempt').subscribe(() => {
        this.socketService.autenticateUser()
      })
      this.pushSubscription();
      this.push.notificationClicks.subscribe(
        ({ action, notification }) => {
          window.open(notification.data.url)
        }
      )
      this.updateClient();
      this.checkUpdate();
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => AppComponent`)
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  updateClient() {
    if (!this.update.isEnabled) {
      console.log('Not Enabled');
      return;
    }
    this.update.available.subscribe((event) => {
      console.log(`current`, event.current, `available `, event.available);
      if (confirm('Hey gracias por usar Riing!! Tenemos una nueva actualización, dale en aceptar para actualizar')) {
        this.update.activateUpdate().then(() => location.reload());
      }
    });

    this.update.activated.subscribe((event) => {
      console.log(`current`, event.previous, `available `, event.current);
    });
  }

  checkUpdate() {
    this.appRef.isStable.subscribe((isStable) => {
      if (isStable) {
        const timeInterval = interval(8 * 60 * 60 * 1000);

        timeInterval.subscribe(() => {
          this.update.checkForUpdate().then(() => console.log('checked'));
          console.log('update checked');
        });
      }
    });
  }

  async pushSubscription() {
    try {
      if (!this.push.isEnabled) {
        console.log('Notificaciones desactivadas')
        return
      }
      let sub = await this.push.requestSubscription({
        serverPublicKey: this.publicKey
      })
      this.webPushBusiness.insertSubscription(sub)
    } catch (error) {
      console.log(error)
    }

  }

}
