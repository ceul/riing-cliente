import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReversePipe } from './reverse.pipe';
import { OrderStatePipe } from './order-state.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ReversePipe,
    OrderStatePipe
  ],
  exports: [
    ReversePipe,
    OrderStatePipe
  ]
})
export class PipesModule {
  static forRoot() {
    return {
      ngModule: PipesModule,
      providers: [],
    };
  }
}
