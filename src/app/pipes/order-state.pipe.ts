import { Pipe, PipeTransform } from '@angular/core';
import OrderStateEnum from '../models/order-state.enum';

@Pipe({
  name: 'orderState'
})
export class OrderStatePipe implements PipeTransform {

  transform(value: any): any {
    try {
      if (value == OrderStateEnum.ORDERED) {
        return 'Recibida Por Negocio'
      } else if (value == OrderStateEnum.PROCESS) {
        return 'En Proceso'
      } else if (value == OrderStateEnum.READY) {
        return 'Lista'
      } else if(value == OrderStateEnum.DELIVERY_MEN_ASIGNED){
        return 'Domiciliario Asignado'
      } else if (value == OrderStateEnum.ON_DELIVERY) {
        return 'Enviada'
      } else if (value == OrderStateEnum.FINISH) {
        return 'Entregada'
      }
    } catch (e) {
      console.log('An error occurred on pipe order state')
    }
  }

}
