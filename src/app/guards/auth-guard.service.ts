import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthBusiness } from '../business/user/auth.business';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router,
    private authBusiness: AuthBusiness) {

  }

  canActivate(): boolean {
    if (!this.authBusiness.isAuthenticated()) {
      this.router.navigateByUrl('/shop');
      return false;
    }
    return true;
  }

}
