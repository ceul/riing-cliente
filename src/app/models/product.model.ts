import OpcionModel from './opcion.model';

export default class ProductModel {

	public constructor(init?: Partial<ProductModel>) {
		if (init) {
			this.id = init.id ;//|| this.id;
			this.almacen = init.almacen;
			this.nombre = init.nombre;
			this.precio_venta = init.precio_venta;
			this.precio_compra = init.precio_compra;
			this.categoria = init.categoria;
			this.imagen = init.imagen;
    		this.descripcion = init.descripcion;
			this.visible = init.visible;
			this.referencia = init.referencia;
			this.opciones = init.opciones;
		} else {
			Object.assign(this, init)
		}
	}

	public id: string;
	public almacen: string;
	public nombre: string;
	public precio_venta: number;
	public precio_compra: number;
	public categoria: string;
	public imagen: string;
    public descripcion: string;
	public visible: boolean;
	public referencia: string;
	public opciones: OpcionModel[]
}
