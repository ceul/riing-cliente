import ItemModel from './items.model';

export default class OpcionModel{
    id: string;
    nombre: string;
    cantidad: number;
    obligatorio: boolean;
    items: ItemModel[];
} 