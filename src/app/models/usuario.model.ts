import TipoUsuarioModel from "./tipo-usuario.model";
import UbicacionModel from './ubicacion.model';

export default class UsuarioModel{
    public id: string;
	public tipo: TipoUsuarioModel;
	public email: string;
	public contrasena: string
	public nombre: string;
	public apellido: string;
	public tel1: string;
	public tel2: string;
	public ubicaciones: UbicacionModel[];
}