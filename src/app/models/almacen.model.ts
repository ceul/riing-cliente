import UsuarioModel from './usuario.model';

export default class AlmacenModel{
	public id: string;
	public usuario: UsuarioModel;
	public pedido_minimo: number;
	public tiempo_minimo: number;
	public tiempo_maximo: number;
	public visible: boolean;
}   
   