export default class ItemModel {
    id: string;
    nombre: string;
    precio: number;
    producto: string;
    seCobra: boolean;
}