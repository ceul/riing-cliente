import ProductModel from './product.model';

export default class CategoryModel {

    public constructor(init?: Partial<CategoryModel >) {
        Object.assign(this, init);
    }

    public id: string;
	public nombre: string;
	public orden: string;
    public almacen: string;
    public productos?: ProductModel[];
}