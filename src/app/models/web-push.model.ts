export default class WebPushModel{
    public id: string;
    public title: string;
    public body: string;
    public url: string;
    public date: Date;
}