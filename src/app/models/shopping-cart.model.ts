import ProductModel from './product.model';
import RelProductoOrdenModel from './rel_producto_orden.model';

export default class ShoppingCartModel{
    public almacen: string;
    public orden: RelProductoOrdenModel[] 
}