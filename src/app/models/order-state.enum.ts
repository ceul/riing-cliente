enum OrderStateEnum {
    ORDERED = 1,
    PROCESS = 2,
    READY = 3,
    DELIVERY_MEN_ASIGNED = 4,
    ON_DELIVERY = 5,
    FINISH = 6,
}
export default OrderStateEnum;