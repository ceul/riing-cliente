enum TipoEnvioEnum {
    DELIVERY = 1,
    TAKE_AWAY = 2,
    ON_TABLE = 3,
}
export default TipoEnvioEnum;