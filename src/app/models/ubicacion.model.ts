export default class UbicacionModel {
    id: string;
	nombre: string;
	latitud: string;
	longitud: string;
	direccion: string;
	usuario: string;
	indicaciones: string;
	tarifa: string
}