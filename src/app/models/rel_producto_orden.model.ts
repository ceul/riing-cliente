import ConfiguracionOpcionItemModel from "./configuracion_opcion_item.model";
import ProductModel from './product.model';
import OpcionModel from './opcion.model';

export default class RelProductoOrdenModel{
    public id: string;
	public producto: ProductModel;
	public orden: string;
	public qty: number;
	public total: number
	public nota: string;
	public items: OpcionModel[]
}