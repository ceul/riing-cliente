import RelProductoOrdenModel from "./rel_producto_orden.model";
import UbicacionModel from './ubicacion.model';

export default class OrdenModel {
    id: string;
	ubicacion: UbicacionModel;
	cliente: string;
	domiciliario: string;
	estado: string;
	hora_inicio:  Date;
	hora_fin: Date;
	serial: number;
	tipo_envio: string;
	productos: RelProductoOrdenModel[]
	tipo_pago: string
}