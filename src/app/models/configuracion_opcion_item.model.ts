export default class ConfiguracionOpcionItemModel{
    public id: string;
    public opcion: string;
    public item: string;
    public seCobra: boolean;
    public qty: number
}