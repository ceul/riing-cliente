import { Component, OnInit, Input, HostListener } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { LoadingSpinnerService } from '../../services/loading-spinner/loading-spinner.service';
import { ProductBusiness } from '../../business/product/product.business';
import ProductModel from '../../models/product.model';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { environment } from '../../../environments/environment';
import OpcionModel from '../../models/opcion.model';
import { ShoppingCartService } from '../../services/shopping-cart/shopping-cart.service';
import RelProductoOrdenModel from '../../models/rel_producto_orden.model';

@Component({
  selector: 'riing-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    this.productDetailModal.hide()
  }

  @Input() productId: string
  @Input() isVisible: boolean
  public product: ProductModel
  public submit: boolean
  public optionsForm: FormGroup
  public selectedOptions: OpcionModel[]

  constructor(public productDetailModal: BsModalRef,
    private loadingService: LoadingSpinnerService,
    private shoppingCartService: ShoppingCartService,
    private productBusiness: ProductBusiness
  ) {
    this.optionsForm = new FormGroup({
      'opciones': new FormArray([]),
      'notas': new FormControl(''),
      'cantidad': new FormControl({ value: 1, disabled: true })
    })
  }

  async ngOnInit() {
    try {
      this.loadingService.show()
      this.selectedOptions = []
      this.submit = false
      this.product = Object.values(await this.productBusiness.getById(this.productId))[0]
      this.product.opciones.forEach((opcion, opindex) => {
        if (opcion !== null && opcion !== undefined) {
          this.addOpcion()
          opcion.items.map((item, itindex) => {
            if (item !== null && item !== undefined && itindex > 0) {
              this.addItem(opindex)
            }
          })
          this.selectedOptions.push({
            id: opcion.id,
            nombre: opcion.nombre,
            cantidad: opcion.cantidad,
            obligatorio: opcion.obligatorio,
            items: [],
          })
        }
      });
      this.optionsForm.patchValue(this.product)

      this.loadingService.hide()
    } catch (error) {
      console.log(error)
      console.log(`An error ocurred on ngOnInit => ProductDetailComponent`)
    }
  }

  public addOpcion() {
    try {
      let control = <FormArray>this.optionsForm.get('opciones')
      control.push(this.initOpcion())
    } catch (error) {
      console.log(`An error occurred adding a Opcion => ProductDetailComponent`)
    }
  }

  public addItem(index) {
    try {
      let control = <FormArray>this.optionsForm.get('opciones')['controls'][index].get('items')
      control.insert(0, this.initItem())
    } catch (error) {
      console.log(`An error occurred adding a item => ProductDetailComponent`)
    }
  }

  public initOpcion() {
    try {
      return new FormGroup({
        id: new FormControl(''),
        nombre: new FormControl('', Validators.required),
        cantidad: new FormControl(1, Validators.required),
        obligatorio: new FormControl(false),
        items: new FormArray([this.initItem()])
      });
    } catch (error) {
      console.log(`An error occurred on initOpcion => ProductDetailComponent`)
    }
  }

  private initItem() {
    try {
      return new FormGroup({
        id: new FormControl(''),
        nombre: new FormControl('', Validators.required),
        precio: new FormControl(0, Validators.required),
        producto: new FormControl(''),
        seCobra: new FormControl(false),
        selected: new FormControl(false)
      });
    } catch (error) {
      console.log(`An error occurred on init Item => ProductDetailComponent`)
    }
  }

  public getopciones(form) {
    try {
      return form.controls.opciones.controls;
    } catch (error) {
      console.log(`An error occurred getting Opciones => ProductDetailComponent`)
    }
  }

  public getItems(form) {
    try {
      return form.controls.items.controls;
    } catch (error) {
      console.log(`An error occurred getting Items => ProductDetailComponent`)
    }
  }

  public getUrl() {
    try {
      return `${environment.url}:${environment.port}/product/getProductImage/${this.productId}`
    } catch (error) {
      console.log(`An error ocurred on getUrl => ProductDetailComponent`)
    }
  }

  public sumQty() {
    try {
      this.optionsForm.controls['cantidad'].setValue(this.optionsForm.controls['cantidad'].value + 1)
    } catch (error) {
      console.log(`An error ocurred on sumQty => ProductDetailComponent`)
    }
  }

  public dismissQty() {
    try {
      let qty = this.optionsForm.controls['cantidad'].value
      if (qty > 1) {
        this.optionsForm.controls['cantidad'].setValue(qty - 1)
      }
    } catch (error) {
      console.log(`An error ocurred on dismissQty => ProductDetailComponent`)
    }
  }

  public onSubmit() {
    try {
      let isCorrect = true
      this.selectedOptions.forEach(option => {
        if (isCorrect) {
          if (option.obligatorio) {
            isCorrect = option.cantidad === option.items.length
          }
        }
      })
      if(isCorrect){
        let productOrder =  new RelProductoOrdenModel()
        productOrder.producto = this.product
        productOrder.qty = this.optionsForm.controls['cantidad'].value
        productOrder.nota = this.optionsForm.controls['notas'].value
        productOrder.items = this.selectedOptions
        productOrder.total = this.calculateTotal()
        this.shoppingCartService.addProduct(this.product.almacen, productOrder)
        this.productDetailModal.hide()
      }
      this.submit = true
    } catch (error) {
      console.log(`An error ocurred on onSubmit => ProductDetailComponent`)
    }
  }

  onSelected(event, optionIndex, option, type) {
    try {
      let target = event.target;
      if (target.checked) {
        if (type == 1) {
          this.selectedOptions[optionIndex].items = []
        }
        this.selectedOptions[optionIndex].items.push(option.value)
      } else {
        let index = this.selectedOptions[optionIndex].items.findIndex(item => item.id === option.value.id)
        if (index !== -1) {
          this.selectedOptions[optionIndex].items.splice(index, 1)
        }
      }
    } catch (error) {
      console.log(`An error ocurred on onSelected => ProductDetailComponent`)
    }
  }

  public radioOnClick(event, optionIndex, option) {
    try {
      let target = event.target;
      if (target.value == 'on') {
        target.value = 1
      } else if (target.value == 1) {
        target.value = parseInt(target.value) + 1
      }

      if (target.checked && parseInt(target.value) == 2) {
        this.selectedOptions[optionIndex].items = []
        target.checked = false
        target.value = 'on'
      } else if (target.checked && parseInt(target.value) > 2) {
        target.value = 'on'
      }
    } catch (error) {
      console.log(`An error ocurred on onSelected => ProductDetailComponent`)
    }
  }

  public calculateTotal() {
    try {
      let itemsTotal = 0
      this.selectedOptions.forEach(option => {
        option.items.forEach(item => {
          if (item.seCobra) {
            itemsTotal = item.precio
          }
        })
      })
      let subTotal = (this.product.precio_venta + itemsTotal)
      return (this.optionsForm.controls['cantidad'].value * subTotal)
    } catch (error) {
      console.log(`An error ocurred on calculateTotal => ProductDetailComponent`)
    }

  }
}
