import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedHelpAdComponent } from './need-help-ad.component';

describe('NeedHelpAdComponent', () => {
  let component: NeedHelpAdComponent;
  let fixture: ComponentFixture<NeedHelpAdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeedHelpAdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeedHelpAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
