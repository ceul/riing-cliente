import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { ToastMessagesComponent } from './toast-messages/toast-messages.component';
import { LottieAnimationViewModule } from 'ng-lottie';
import { OrderSideDescriptionComponent } from './order-side-description/order-side-description.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NeedHelpAdComponent } from './need-help-ad/need-help-ad.component';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { NewsModalComponent } from './news-modal/news-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';
@NgModule({
  declarations: [
    LoadingSpinnerComponent,
    OrderSideDescriptionComponent,
    ProductDetailComponent,
    NeedHelpAdComponent,
    NewsModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    LottieAnimationViewModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    ModalModule,
    NgSelectModule
  ],
  exports: [
    LoadingSpinnerComponent,
    OrderSideDescriptionComponent,
    ProductDetailComponent,
    NeedHelpAdComponent,
    NewsModalComponent
  ],
  providers: [
    BsModalService
  ],
})
export class ComponentsModule { }
