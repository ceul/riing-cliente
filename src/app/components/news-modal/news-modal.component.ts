import { Component, OnInit, HostListener } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { LoadingSpinnerService } from '../../services/loading-spinner/loading-spinner.service';
import { Router } from '@angular/router';

@Component({
  selector: 'riing-news-modal',
  templateUrl: './news-modal.component.html',
  styleUrls: ['./news-modal.component.scss']
})
export class NewsModalComponent implements OnInit {

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    this.newsModal.hide()
  }

  constructor(public newsModal: BsModalRef,
    private loadingService: LoadingSpinnerService,
    private router: Router
  ) {
  }

  async ngOnInit() {
    try {

    } catch (error) {
      console.log(`An error ocurred on ngOnInit => NewsModalComponent`)
    }
  }

  public onSubmit() {
    try {
      this.newsModal.hide()
      this.router.navigateByUrl(`/shop/products/921c0efe-9cf6-4d42-b8c7-144295f1ae60/1`)
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => onSubmit`)
    }
  }

}