import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, ViewChild } from '@angular/core';
import ProductModel from '../../models/product.model';
import ShoppingCartModel from '../../models/shopping-cart.model';
import { ShoppingCartService } from '../../services/shopping-cart/shopping-cart.service';
import { Subscription } from 'rxjs';
import RelProductoOrdenModel from '../../models/rel_producto_orden.model';
import { Router } from '@angular/router';
import { TarifasBusiness } from '../../business/tarifas/tarifas.business';
import TarifaModel from '../../models/tarifa.model';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { AuthBusiness } from '../../business/user/auth.business';
import { take } from 'rxjs/operators';
import { LoginComponent } from '../../modules/user/login/login.component';
import { AuthService } from '../../services/user/auth.service';
import TipoEnvioEnum from '../../models/tipo_envio.enum';

@Component({
  selector: 'riing-order-side-description',
  templateUrl: './order-side-description.component.html',
  styleUrls: ['./order-side-description.component.scss']
})
export class OrderSideDescriptionComponent implements OnInit, OnDestroy {

  @Input() origin: string
  /* product-detail - payment*/
  @Input() almacenId: string
  @Input() almacenNombre: string
  @Input() pedidoMinimo: number
  @Input() shopType: string
  @Input() orderStep: number
  @Input() tarifa: TarifaModel
  @Input() onTable: boolean
  @Input() isVisible: boolean
  @Output() nextBtnClick: EventEmitter<any> = new EventEmitter()
  @Output() forwardBtnClick: EventEmitter<any> = new EventEmitter()
  @Output() deliveryWayEmitter: EventEmitter<string> = new EventEmitter()

  @ViewChild('selectProducts', { static: true }) public selectProductsModal: ModalDirective;
  @ViewChild('minimumOrder', { static: true }) public minimumOrderModal: ModalDirective;
  @ViewChild('mobileOrderSide', { static: true }) public mobileOrderSideModal: ModalDirective;
  @ViewChild('shopClosed', { static: true }) public shopClosedModal: ModalDirective;


  public products: RelProductoOrdenModel[]
  public tarifas: TarifaModel[]
  public tarifaSelectedId: string
  private productListener: Subscription
  public subTotal: number
  public total: number
  public deliveryWay: string
  private loginModal: BsModalRef
  public submit: boolean

  constructor(private shoppingCartService: ShoppingCartService,
    private tarifaBusiness: TarifasBusiness,
    private modalService: BsModalService,
    private authBusiness: AuthBusiness,
    private authService: AuthService,
    private router: Router) { }

  async ngOnInit() {
    try {
      this.products = []
      this.tarifas = []
      this.tarifaSelectedId = null
      this.submit = false
      if (this.tarifa === null || this.tarifa === undefined) {
        this.tarifa = new TarifaModel()
        this.tarifa.valor = 0
        this.tarifa.id = null
      }
      if (!this.onTable) {
        let deliveryWayTmp = localStorage.getItem('deliveryWay')
        if (deliveryWayTmp !== null && deliveryWayTmp !== undefined && deliveryWayTmp !== null) {
          this.deliveryWay = deliveryWayTmp
        } else {
          this.deliveryWay = TipoEnvioEnum.DELIVERY.toString()
        }
      } else {
        this.deliveryWay = TipoEnvioEnum.ON_TABLE.toString()
      }

      this.deliveryWayEmitter.emit(this.deliveryWay)
      this.total = 0
      this.subTotal = 0
      this.tarifas = Object.values(await this.tarifaBusiness.get())
      this.getTarifa()
      this.getShoppingCart()
      this.calculateTotal()
      this.productListener = this.shoppingCartService.emitProduct.subscribe(product => {
        this.getShoppingCart()
        this.calculateTotal()
      })
    } catch (erorr) {
      console.log(`An error ocurred on ngOnInit => OrderSideDescriptionComponent`)
    }
  }

  ngOnDestroy() {
    try {
      this.productListener.unsubscribe()
    } catch (erorr) {
      console.log(`An error ocurredo on ngOnDestroy => OrderSideDescriptionComponent`)
    }
  }

  ngOnChanges(changes) {
    try {
      if (changes.tarifa) {
        this.tarifa = changes.tarifa.currentValue
        this.tarifaSelectedId = this.tarifa.id
        this.tarifaChange()
      }
    } catch (e) {
      console.log('An error occurred changing the catalog list e', e)
    }
  }

  getTarifa() {
    try {
      let tarifa = <TarifaModel>JSON.parse(localStorage.getItem('tarifa'))
      if (tarifa !== null && tarifa !== undefined) {
        this.tarifa = tarifa
        this.tarifaSelectedId = this.tarifa.id
      }
    } catch (erorr) {
      console.log(`An error ocurred on getTarifa => OrderSideDescriptionComponent`)
    }
  }

  getShoppingCart() {
    try {
      let shoppingCart = <ShoppingCartModel[]>JSON.parse(localStorage.getItem('shoppingCart'))
      if (shoppingCart !== null && shoppingCart !== undefined && shoppingCart.length > 0) {
        let index = shoppingCart.findIndex(shop => shop.almacen === this.almacenId)
        if (index !== -1) {
          this.products = shoppingCart[index].orden
        }
      } else {
        this.products = []
      }
    } catch (erorr) {
      console.log(`An error ocurred on getShoppingCart => OrderSideDescriptionComponent`)
    }
  }

  calculateTotal() {
    try {
      this.subTotal = 0
      if (this.products !== undefined) {
        for (let index = 0; index < this.products.length; index++) {
          this.subTotal = this.subTotal + this.products[index].total
        }
      }
      this.total = this.subTotal
      if (this.deliveryWay == TipoEnvioEnum.DELIVERY.toString()) {
        this.total = this.total + this.tarifa.valor
      }
    } catch (erorr) {
      console.log(`An error ocurredo on calculateTotal => OrderSideDescriptionComponent`)
    }
  }

  deleteProduct(index) {
    try {
      this.shoppingCartService.deleteProduct(this.almacenId, index)
    } catch (erorr) {
      console.log(`An error ocurred on deleteProduct => OrderSideDescriptionComponent`)
    }
  }

  returnToShop() {
    try {
      this.router.navigateByUrl(`/shop/products/${this.almacenId}/${this.shopType}`)
    } catch (erorr) {
      console.log(`An error ocurred on returnToShop => OrderSideDescriptionComponent`)
    }
  }

  order() {
    try {
      if (this.isVisible) {
        if (this.tarifaSelectedId !== null) {

          if (this.authBusiness.isAuthenticated()) {
            if (this.products.length > 0) {
              if (this.subTotal < this.pedidoMinimo && !this.onTable) {
                this.minimumOrderModal.show()
              } else {
                if (this.onTable) {
                  this.router.navigateByUrl(`/order/${this.almacenId}/${this.shopType}/t`)
                } else {
                  this.router.navigateByUrl(`/order/${this.almacenId}/${this.shopType}`)
                }
              }
            } else {
              this.selectProductsModal.show()
            }
          } else {
            this.openLoginModal()
          }
        } else {
          this.submit = true
        }
      } else {
        this.shopClosedModal.show()
      }
    } catch (erorr) {
      console.log(`An error ocurred on order => OrderSideDescriptionComponent`)
    }
  }

  openLoginModal() {
    try {
      this.loginModal = this.modalService.show(LoginComponent, { class: 'modal-lg modal-dialog-centered', animated: true })
      this.loginModal.content.isLogged.pipe(take(1)).subscribe(isLogged => {
        if (isLogged) {
          this.authService.isLoggedFunc(isLogged)
          this.order()
        }
      })
    } catch (error) {
      console.log(`An error ocurred on openLoginModal => OrderSideDescriptionComponent`)
    }
  }

  nextStep() {
    try {
      this.nextBtnClick.emit()
    } catch (erorr) {
      console.log(`An error ocurred on nextStep => OrderSideDescriptionComponent`)
    }
  }

  forwardStep() {
    try {
      this.forwardBtnClick.emit()
    } catch (erorr) {
      console.log(`An error ocurred on nextStep => OrderSideDescriptionComponent`)
    }
  }

  public tarifaChange() {
    try {
      if (this.tarifaSelectedId !== null && this.tarifas !== undefined) {
        this.tarifa = this.tarifas.find(tarifa => tarifa.id == this.tarifaSelectedId)
        this.submit = false
      } else {
        this.tarifa = new TarifaModel()
        this.tarifa.valor = 0
        this.tarifa.id = null
      }
      localStorage.setItem('tarifa', JSON.stringify(this.tarifa))
      this.calculateTotal()
    } catch (erorr) {
      console.log(`An error ocurred on tarifaChange => OrderSideDescriptionComponent`)
    }
  }

  deliveryWayChange() {
    try {
      this.calculateTotal()
      localStorage.setItem('deliveryWay', this.deliveryWay.toString())
      this.deliveryWayEmitter.emit(this.deliveryWay)
    } catch (erorr) {
      console.log(`An error ocurred on deliveryWayChange => OrderSideDescriptionComponent`)
    }
  }

}
