import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderSideDescriptionComponent } from './order-side-description.component';

describe('OrderSideDescriptionComponent', () => {
  let component: OrderSideDescriptionComponent;
  let fixture: ComponentFixture<OrderSideDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderSideDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSideDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
