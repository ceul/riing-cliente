// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: 'http://127.0.0.1',
  port: '3100',
  activeLang: 'es',
  firebaseConfig : {
    apiKey: "AIzaSyApZctllb7ZDSRJ-EdXP65Zz-tuQcHMLDU",
    authDomain: "riing-302fc.firebaseapp.com",
    databaseURL: "https://riing-302fc.firebaseio.com",
    projectId: "riing-302fc",
    storageBucket: "riing-302fc.appspot.com",
    messagingSenderId: "960179652739",
    appId: "1:960179652739:web:565bf8568fc5ba5707f3b6",
    measurementId: "G-LW4EMBSZW5"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
